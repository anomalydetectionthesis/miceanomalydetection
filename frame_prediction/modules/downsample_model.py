import torch
from torch import nn

from frame_prediction.modules.Generator import Unet


class DownsampleModel(nn.Module):
    def __init__(self, unet: Unet, device= torch.device('cpu')):
        super(DownsampleModel, self).__init__()

        self.device = device
        # construct unet structure
        self.downsample_layers = nn.Sequential(*unet.downsample_layers).to(self.device)

    def forward(self, input):
        latent_out = self.downsample_layers.forward(input)
        return latent_out

    @torch.no_grad()
    def predict(self, past_frame: torch.Tensor) -> torch.Tensor:
        assert past_frame.ndim == 4  # shape must be [batch_dim, channel_dim, height, width]
        assert past_frame.shape[0] == 1  # batch size must be 1

        latent_vect = self.downsample_layers(past_frame.to(self.device))

        return latent_vect.cpu()
