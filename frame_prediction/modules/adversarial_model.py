import torch

from frame_prediction.modules.Discriminator import PatchDiscriminator
from frame_prediction.modules.Generator import Unet

N_IMG_CHANNELS = 'n_img_channels'
N_STACKED_FRAMES = 'n_stacked_frames'
GEN_STATE_DICT = 'gen_state_dict'
DISCR_STATE_DICT = 'discr_state_dict'

class AdversarialModel():
    def __init__(self, img_channels, n_stacked_frames, device = torch.device('cpu')):
        gen_in_channels = img_channels*n_stacked_frames

        self.gen_net = Unet(in_channels=gen_in_channels, out_channels=img_channels, channels=(64, 128, 256, 512)).to(device)
        self.discr_net = PatchDiscriminator(in_channels=img_channels).to(device)
        self.img_channels = img_channels
        self.n_stacked_frames = n_stacked_frames
        self.device = device

    @torch.no_grad()
    def predict(self, past_frame: torch.Tensor) -> torch.Tensor:
        assert past_frame.ndim == 4 #shape must be [batch_dim, channel_dim, height, width]
        assert past_frame.shape[0] == 1 #batch size must be 1
        self.eval_mode()

        pred_frame = self.gen_net(past_frame.to(self.device))

        return pred_frame.cpu()

    def train_mode(self):
        self.gen_net.train()
        self.discr_net.train()

    def eval_mode(self):
        self.gen_net.eval()
        self.discr_net.eval()

    @staticmethod
    def from_ckpt(ckpt_dict, device= torch.device('cpu')):

        adv_model = AdversarialModel(ckpt_dict[N_IMG_CHANNELS], ckpt_dict[N_STACKED_FRAMES], device=device)
        adv_model.gen_net.load_state_dict(ckpt_dict[GEN_STATE_DICT])
        adv_model.discr_net.load_state_dict(ckpt_dict[DISCR_STATE_DICT])

        return adv_model

    def get_state_dict(self):
        state = {}
        state[N_IMG_CHANNELS] = self.img_channels
        state[N_STACKED_FRAMES] = self.n_stacked_frames
        state[GEN_STATE_DICT] = self.gen_net.state_dict()
        state[DISCR_STATE_DICT] = self.discr_net.state_dict()
        return state

    def save_checkpoint(self, checkpoint_path):
        torch.save(self.get_state_dict(), checkpoint_path)