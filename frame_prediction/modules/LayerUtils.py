from collections.abc import Iterable

def pad_same(kernel_size: int or tuple) -> int or tuple:
    """ Compute the padding required in order to have the output
    shape equals to the input shape under the assumption of stride = 1

    Args:
        kernel_size (int or tuple): The kernel shape
    Return:
        (int or tuple): the padding value
    """
    def compute_pad(k: int)-> int:
        return (k-1)//2

    if isinstance(kernel_size, Iterable):
        return tuple(map(compute_pad, kernel_size))
    elif isinstance(kernel_size, int):
        return compute_pad(kernel_size)
    else:
        raise ValueError("The kernel_size parameter must be int or tuple of int")

if __name__ == '__main__':
    print(pad_same((3, 3)))


