import torch
from torch import nn
from torch.nn.functional import leaky_relu
from torchsummary import summary

from frame_prediction.modules.LayerUtils import pad_same


def _downsample_layers(in_channels, channels: tuple, kernel_size):
    layers = [UnetConvolution(in_channels=in_channels, out_channels=channels[0], kernel_size=kernel_size)]
    for i in range(0, len(channels)-1):
        c_in = channels[i]
        c_out = channels[i+1]
        layers.append(UnetDownsample(in_channels=c_in, out_channels=c_out, kernel_size=kernel_size))

    return layers


def _upsample_layers(channels: tuple, kernel_size):
    layers = []
    for i in reversed(range(0, len(channels)-1)):
        c_in = channels[i+1]
        c_out = channels[i]
        layers.append(UnetUpsample(in_channels=c_in, out_channels=c_out, kernel_size=kernel_size))

    return layers


class Unet(nn.Module):

    def __init__(self, in_channels, out_channels, channels=(64, 128, 256, 512), kernel_size=(3,3)):
        """Unet module
        """
        super(Unet, self).__init__()

        # construct unet structure
        self.downsample_layers = nn.ModuleList(_downsample_layers(in_channels, channels, kernel_size=kernel_size))
        self.upsample_layers = nn.ModuleList(_upsample_layers(channels, kernel_size=kernel_size))
        self.out_block = Conv2dBlock(in_channels=channels[0], out_channels=out_channels, kernel_size=kernel_size)


    def forward(self, input):
        #Downsampling
        downsample_out = []
        next_input = input
        for layer in self.downsample_layers:
            layer_out = layer(next_input)
            next_input =layer_out
            downsample_out.append(layer_out)

        # Upsampling
        for i in range(0,len(downsample_out)-1):
            shortcut = downsample_out[len(downsample_out)-2-i]
            layer = self.upsample_layers[i]
            next_input = layer(next_input, shortcut)

        # out
        return self.out_block(next_input)


class Conv2dBlock(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size):
        super(Conv2dBlock, self).__init__()

        self.block = nn.Sequential(
            nn.Conv2d(
                in_channels=in_channels,
                out_channels=out_channels,
                kernel_size=kernel_size,
                padding=pad_same(kernel_size)),
            nn.LeakyReLU(),
        )

    def forward(self, input):
        return self.block(input)


class UnetConvolution(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size):
        super(UnetConvolution, self).__init__()

        self.block = nn.Sequential(
            Conv2dBlock(in_channels=in_channels, out_channels=out_channels, kernel_size=kernel_size),
            Conv2dBlock(in_channels=out_channels, out_channels=out_channels, kernel_size=kernel_size),
        )

    def forward(self, input):
        return self.block(input)


class UnetDownsample(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size, pool_kernel_size=(2,2)):
        super(UnetDownsample, self).__init__()

        self.max_pooling = nn.MaxPool2d(kernel_size=pool_kernel_size, padding=pad_same(pool_kernel_size))
        self.unet_conv = UnetConvolution(in_channels, out_channels, kernel_size=kernel_size)

    def forward(self, input):
        downsampled_input = self.max_pooling(input)
        return self.unet_conv(downsampled_input)


class UnetUpsample(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size):
        super(UnetUpsample, self).__init__()

        self.upconv_block = nn.ConvTranspose2d(
            in_channels,
            out_channels,

            kernel_size=kernel_size,
            stride=2,
            padding=1,
            output_padding=1
        )
        self.unet_conv = UnetConvolution(in_channels, out_channels, kernel_size=kernel_size)

    def forward(self, input: torch.Tensor, shortcut: torch.Tensor):
        upconv_out = leaky_relu(self.upconv_block(input))
        unet_in = torch.cat([shortcut, upconv_out], 1)
        block_out = self.unet_conv(unet_in)
        return block_out


if __name__ == '__main__':
    model = Unet(in_channels=10, out_channels=1, channels=(64, 128, 256, 512))
    summary(model=model,input_size=(10, 104, 104), batch_size=1)
