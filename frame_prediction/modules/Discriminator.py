from torch import nn
from torchsummary import summary

from frame_prediction.modules.LayerUtils import pad_same


class PatchDiscriminator(nn.Module):
    """70X70 patch discriminator
    C64-C128-C256-C512
    kernel 4x4
    stride 2
    """
    def __init__(self, in_channels):
        super(PatchDiscriminator, self).__init__()
        channels = [64, 128, 256, 512]
        self.net = nn.Sequential(
            PatchConv2dBlock(in_channels, channels[0]),
            PatchConv2dBlock(channels[0], channels[1]),
            PatchConv2dBlock(channels[1], channels[2]),
            PatchConv2dBlock(channels[2], channels[3]),
            PatchOutBlock(in_channels=channels[3])
        )

    def forward(self, input):
        return self.net(input)


class PatchOutBlock(nn.Module):
    def __init__(self, in_channels):
        super(PatchOutBlock, self).__init__()

        self.block = nn.Sequential(
            nn.Conv2d(
                in_channels=in_channels,
                out_channels=1,
                kernel_size=1),
            nn.Sigmoid(),
        )

    def forward(self, input):
        return self.block(input)


class PatchConv2dBlock(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size=(4, 4), stride=2):
        super(PatchConv2dBlock, self).__init__()

        self.block = nn.Sequential(
            nn.Conv2d(
                in_channels=in_channels,
                out_channels=out_channels,
                kernel_size=kernel_size,
                stride=stride,
                padding=pad_same(kernel_size)),
            nn.BatchNorm2d(num_features=out_channels),
            nn.LeakyReLU(negative_slope=0.2),
        )

    def forward(self, input):
        return self.block(input)


if __name__ == '__main__':
    in_channels=1
    model = PatchDiscriminator(in_channels=in_channels)
    summary(model=model,input_size=(in_channels, 104, 104), batch_size=1)