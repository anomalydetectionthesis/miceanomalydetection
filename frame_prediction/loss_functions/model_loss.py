import torch

from frame_prediction.loss_functions.adversarial_loss import generator_loss, discriminator_loss
from frame_prediction.loss_functions.reconstruction_loss import intensity_loss, gradient_loss


class UnetLoss:
    def __init__(self, intensity_weight=1, gradient_weight=2, adversarial_weight=0.05):
        self.intensity_weight=intensity_weight
        self.gradient_weight = gradient_weight
        self.adversarial_weight = adversarial_weight

    def __call__(self, target_imgs, fake_imgs, fake_patches):
        int_loss = self.intensity_weight * intensity_loss(target_imgs, fake_imgs)
        grad_loss = self.gradient_weight * gradient_loss(target_imgs, fake_imgs)
        gen_patch_loss = self.adversarial_weight * generator_loss(fake_patches)

        return int_loss + grad_loss + gen_patch_loss, int_loss, grad_loss, gen_patch_loss


class DiscriminatorLoss:
    def __call__(self, target_patch_matrix: torch.FloatTensor, fake_patch_matrix: torch.FloatTensor):
        return discriminator_loss(target_patch_matrix, fake_patch_matrix)