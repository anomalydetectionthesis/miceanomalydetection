import torch
from torch import nn

def _v_grad(img: torch.FloatTensor):
    return (img[:,:, 1:, :] - img[:,:, 0:-1, :]).abs()


def _h_grad(img: torch.FloatTensor):
    return (img[:,:, :, 1:] - img[:,:, :, 0:-1]).abs()


def intensity_loss(target_img_batch: torch.FloatTensor, reconstructed_img_batch: torch.FloatTensor):
    """The intensity loss between two images is the l2_norm
    """
    assert target_img_batch.shape == reconstructed_img_batch.shape

    batch_shape = target_img_batch.shape
    img_dims = tuple(range(1, len(batch_shape)))

    sse = torch.sum((target_img_batch-reconstructed_img_batch)**2, dim=img_dims)
    return torch.mean(sse)


def gradient_loss(target_img: torch.FloatTensor, reconstructed_img: torch.FloatTensor):
    assert target_img.shape == reconstructed_img.shape

    batch_size = len(target_img)

    l1_loss_fn = nn.L1Loss(reduction='sum')

    v_loss = l1_loss_fn(_v_grad(reconstructed_img), _v_grad(target_img))
    h_loss = l1_loss_fn(_h_grad(reconstructed_img), _h_grad(target_img))
    return (v_loss+h_loss)/batch_size