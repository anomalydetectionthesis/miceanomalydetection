import torch

def _adversarial_loss(patch_matrix: torch.FloatTensor, expected : int):
    """
    Args
    - patch_matrix:
    - expected (int): must be 0 or 1
    """
    loss = (patch_matrix - expected)**2
    return torch.sum(loss, dim=tuple(range(1,loss.ndim)))


def discriminator_loss(target_patch_matrix: torch.FloatTensor, fake_patch_matrix: torch.FloatTensor):
    target_loss = 0.5*_adversarial_loss(target_patch_matrix, expected=1)
    fake_loss = 0.5 * _adversarial_loss(fake_patch_matrix, expected=0)
    return torch.mean(target_loss + fake_loss), torch.mean(target_loss), torch.mean(fake_loss)


def generator_loss(fake_patch_matrix: torch.FloatTensor):
    fake_loss = 0.5*_adversarial_loss(fake_patch_matrix, expected=1)
    return torch.mean(fake_loss)
