import os
import time

import torch
from torch.utils.data import DataLoader

from dataset.base.dataset_model import BaseDataset
from frame_prediction.engine.train.train_progress import TrainProgressPrinter
from frame_prediction.engine.train.trainer import AdvTrainer
from frame_prediction.modules.adversarial_model import AdversarialModel


class TrainEngine:
    def __init__(self, trainer:AdvTrainer):

        self.trainer = trainer

        self.current_epoch = 0
        self.gen_loss_history = []
        self.discr_loss_history = []
        self.train_duration = 0

    def fit(self, epochs, train_ds: BaseDataset, batch_size, progress_printer: TrainProgressPrinter, ckpt_dir=None):
        dl = DataLoader(train_ds, batch_size=batch_size,shuffle=True)

        batch_num = len(dl)
        print(f'There are {batch_num} batches')

        print("=" * 30)
        train_epochs = range(self.current_epoch + 1, epochs+1)
        for epoch in train_epochs:
            t0 = time.time()
            self.current_epoch = epoch
            current_batch = 0

            epoch_gen_loss = {}
            epoch_discr_loss = {}

            for trainer_input in dl:
                b0 = time.time()
                current_batch += 1
                pred_frame, pred_gen_loss, pred_discr_loss = self.trainer.train_step(*trainer_input)

                # Update loss stats
                self._update_loss_dict(epoch_gen_loss, pred_gen_loss)
                self._update_loss_dict(epoch_discr_loss, pred_discr_loss)

                # Print train info
                mean_gen_loss, mean_discr_loss = self._get_loss_stats(epoch_gen_loss, epoch_discr_loss, current_batch)
                batch_duration = time.time() - b0
                progress_printer.print_batch_info(epochs, self.current_epoch, batch_num, current_batch, batch_duration, mean_gen_loss, mean_discr_loss)


            epoch_duration = time.time() - t0
            self.train_duration += epoch_duration

            mean_gen_loss, mean_discr_loss =  self._get_loss_stats(epoch_gen_loss, epoch_discr_loss, batch_num)

            self.gen_loss_history.append(mean_gen_loss)
            self.discr_loss_history.append(mean_discr_loss)

            progress_printer.print_epoch_info(epochs, self.current_epoch, epoch_duration, mean_gen_loss, mean_discr_loss)

            self._save_checkpoints(ckpt_dir)

        fit_summary = self._fit_summary()
        self.train_duration = 0
        return fit_summary

    def _update_loss_dict(self, current_loss_dict, pred_loss_dict):
        for key in pred_loss_dict.keys():
            if key not in current_loss_dict:
                current_loss_dict[key] = 0
            current_loss_dict[key] += pred_loss_dict[key]

    def _get_loss_stats(self, gen_loss_dict_sum: dict, discr_loss_dict_sum: dict, batch_num):
        gen_loss_dict = {}
        discr_loss_dict= {}

        for key, loss_sum in gen_loss_dict_sum.items():
            gen_loss_dict[key] = loss_sum/batch_num

        for key, loss_sum in discr_loss_dict_sum.items():
            discr_loss_dict[key] = loss_sum/batch_num

        return gen_loss_dict, discr_loss_dict

    @staticmethod
    def resume_fit(train_ckpt_dict, device= torch.device('cpu')):

        trainer = AdvTrainer.from_ckpt(train_ckpt_dict['trainer_ckpt_dict'], device)

        train_eng = TrainEngine(trainer)
        fit_summary = train_ckpt_dict['fit_summary']
        train_eng.current_epoch = fit_summary['epoch']
        train_eng.train_duration = fit_summary['train_time']
        train_eng.gen_loss_history = fit_summary['gen_loss']
        train_eng.discr_loss_history = fit_summary['discr_loss']

        return train_eng

    def _save_checkpoints(self, ckpt_dir):
        if ckpt_dir is not None:
            model_ckpt_path = os.path.join(ckpt_dir, f'{self.current_epoch}_model_ckpt.pth')
            train_ckpt_path = os.path.join(ckpt_dir, f'{self.current_epoch}_train_ckpt.pth')

            train_status = {}
            train_status['fit_summary'] = self._fit_summary()
            train_status['trainer_ckpt_dict'] = self.trainer.get_state_dict()

            adv_model: AdversarialModel = self.trainer.get_model()
            model_state_dict = adv_model.get_state_dict()

            # save model parameters
            torch.save(model_state_dict, model_ckpt_path)
            torch.save(train_status, train_ckpt_path)

    def _fit_summary(self):
        return {
            'epoch': self.current_epoch,
            'train_time': self.train_duration,
            'gen_loss': self.gen_loss_history,
            'discr_loss': self.discr_loss_history,
        }