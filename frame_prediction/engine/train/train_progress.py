from plot_utils import img_compare

class TrainProgressPrinter:
    def __init__(self, verbose=False, verbose_freq=50, example_freq=50):
        self.verbose = verbose
        self.verbose_freq = verbose_freq
        self.example_freq = example_freq

    def _show_prediction_example(self, gt_frame, pred_frame):
        img_compare(gt_frame, pred_frame)

    def print_batch_info(self, epochs, current_epoch, batches, current_batch, batch_duration, gen_loss_dict, discr_loss_dict):
        if self.verbose and current_batch%self.verbose_freq == 0:
            epochs_info = f"[{current_epoch}/{epochs}]"
            batch_info = f"[{current_batch}/{batches}]"
            batch_duration_info = f"{'%.5f' % batch_duration}sec"
            gen_loss_dict_info, discr_loss_dict_info = self._loss_stats_to_string(gen_loss_dict, discr_loss_dict)

            print(f"{epochs_info}{batch_info} {batch_duration_info} -> {discr_loss_dict_info}\t{gen_loss_dict_info}")

    def print_epoch_info(self, epochs, current_epoch, epoch_duration, gen_loss_dict, discr_loss_dict):
        epochs_info = f"[{current_epoch}/{epochs}]"
        epoch_duration_info = f"{'%.5f' % epoch_duration}sec"

        gen_loss_dict_info, discr_loss_dict_info = self._loss_stats_to_string(gen_loss_dict, discr_loss_dict)

        print(f"{epochs_info} {epoch_duration_info} -> {discr_loss_dict_info}\t {gen_loss_dict_info}")

    def _loss_stats_to_string(self, gen_loss_dict, discr_loss_dict):
        for key in gen_loss_dict.keys():
            gen_loss_dict[key] = f"{'%.5f' % gen_loss_dict[key]}"

        for key in discr_loss_dict.keys():
            discr_loss_dict[key] = f"{'%.5f' % discr_loss_dict[key]}"

        return str(gen_loss_dict), str(discr_loss_dict)