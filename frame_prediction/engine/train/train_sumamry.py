import os

import numpy as np
import torch
from tabulate import tabulate

from paths import ROOT_PATH


class TrainSummary:
    def __init__(self, train_ckpt_dict):
        self.train_ckpt_dict = train_ckpt_dict
        self.fit_summary = self.train_ckpt_dict['fit_summary']

        self.epochs = self.fit_summary['epoch']
        self.train_duration = self.fit_summary['train_time']
        self.gen_loss_history = self.fit_summary['gen_loss']
        self.discr_loss_history = self.fit_summary['discr_loss']

    def _get_rounded_loss(self, loss):
        return  f"{'%.5f' % float(loss)}"

    def loss_dict_to_string(self,loss_dict, main_loss_id, secondary_loss_ids = None):
        main_loss = self._get_rounded_loss(loss_dict[main_loss_id])

        if secondary_loss_ids is not None:
            secondary_loss = [self._get_rounded_loss(loss_dict[loss_id]) for loss_id in secondary_loss_ids]
            secondary_loss_str = str.join(', ', secondary_loss)

            return f"{main_loss} ( {secondary_loss_str} )"
        else:
            return f"{main_loss}"

    def show_loss_table(self):
        gen_loss_id = 'gen_loss'
        discr_loss_id = 'discr_loss'
        epoch_ids = [epoch +1 for epoch in range(0,self.epochs)]
        print(self.discr_loss_history[0].keys())
        gen_loss = [self.loss_dict_to_string(loss_dict, gen_loss_id, ('int_loss', 'grad_loss', 'adv_loss')) for loss_dict in self.gen_loss_history]
        discr_loss = [self.loss_dict_to_string(loss_dict, discr_loss_id, ('target_loss', 'fake_loss')) for loss_dict in self.discr_loss_history]

        summary = np.column_stack((epoch_ids, gen_loss, discr_loss))

        print(tabulate(summary, headers=('epoch', 'gen_loss (int_loss, grad_loss, adv_loss)', 'discr_loss (target_loss, fake_loss)')))

if __name__ == '__main__':

    train_ckpt_path = os.path.join(ROOT_PATH, 'data/mice/sf10_glr1e-4_dlr1e-5_loss_i1_w1_a1/ckpt/20_train_ckpt.pth')
    summary = TrainSummary(torch.load(train_ckpt_path, map_location=torch.device('cpu')))
    summary.show_loss_table()