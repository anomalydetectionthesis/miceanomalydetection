import numpy as np
from torch import optim

from frame_prediction.loss_functions.model_loss import UnetLoss, DiscriminatorLoss
from frame_prediction.modules.adversarial_model import AdversarialModel


class AdvTrainer:
    def __init__(self, adv_model: AdversarialModel, gen_lr, discr_lr, intensity_weight=1, gradient_weight=1, adversarial_weight=1):

        self.adv_model = adv_model
        self.device = self.adv_model.device
        self.gen_lr = gen_lr
        self.discr_lr = discr_lr
        self.int_weight = intensity_weight
        self.grad_weight = gradient_weight
        self.adv_weight = adversarial_weight

        self.gen_opt = optim.Adam(params=self.adv_model.gen_net.parameters(), lr=self.gen_lr)
        self.discr_opt = optim.Adam(params=self.adv_model.discr_net.parameters(), lr=self.discr_lr)

        self.gen_loss_fn = UnetLoss(intensity_weight=intensity_weight, gradient_weight=gradient_weight, adversarial_weight=adversarial_weight)
        self.discr_loss_fn = DiscriminatorLoss()

    def get_model(self):
        return self.adv_model

    def train_step(self, prev_frames, next_frame):
        self.adv_model.train_mode()

        prev_frames = prev_frames.to(self.device)
        next_frame = next_frame.to(self.device)

        self._set_requires_grad(self.adv_model.discr_net, True)

        # forward
        fake_imgs = self.adv_model.gen_net(prev_frames)
        fake_patches = self.adv_model.discr_net(fake_imgs.detach())
        real_patches = self.adv_model.discr_net(next_frame)

        # compute loss
        discr_loss, target_loss, fake_loss = self.discr_loss_fn(real_patches, fake_patches)
        self._backward(discr_loss, self.discr_opt)


        self._set_requires_grad(self.adv_model.discr_net, False)
        fake_patches = self.adv_model.discr_net(fake_imgs)

        gen_loss, int_loss, grad_loss, adv_loss = self.gen_loss_fn(next_frame, fake_imgs, fake_patches)
        self._backward(gen_loss, self.gen_opt)

        return (
            fake_imgs.cpu().detach().numpy(),
            {
                'gen_loss': np.asscalar(gen_loss.cpu().detach().numpy()),
                'int_loss': np.asscalar(int_loss.cpu().detach().numpy()),
                'grad_loss':np.asscalar(grad_loss.cpu().detach().numpy()),
                'adv_loss': np.asscalar(adv_loss.cpu().detach().numpy()),
            },
            {
                'discr_loss': np.asscalar(discr_loss.cpu().detach().numpy()),
                'target_loss': np.asscalar(target_loss.cpu().detach().numpy()),
                'fake_loss': np.asscalar(fake_loss.cpu().detach().numpy())
            }
        )

    def get_state_dict(self):
        return {
            'model_ckpt_dict': self.adv_model.get_state_dict(),
            'gen_lr': self.gen_lr,
            'discr_lr': self.discr_lr,
            'int_weight': self.int_weight,
            'grad_weight': self.grad_weight,
            'adv_weight': self.adv_weight,
            'gen_opt' : self.gen_opt.state_dict(),
            'discr_opt' : self.discr_opt.state_dict()
        }

    def _backward(self, loss, opt):
        opt.zero_grad()
        loss.backward()
        opt.step()

    def _set_requires_grad(self, model, requires_grad: bool):
        for par in model.parameters():
            par.requires_grad = requires_grad

    @staticmethod
    def from_ckpt(ckpt_dict, device):
        adv_model = AdversarialModel.from_ckpt(ckpt_dict['model_ckpt_dict'], device)

        trainer = AdvTrainer(
            adv_model,
            ckpt_dict['gen_lr'],
            ckpt_dict['discr_lr'],
            ckpt_dict['int_weight'],
            ckpt_dict['grad_weight'],
            ckpt_dict['adv_weight']
        )

        trainer.gen_opt.load_state_dict(ckpt_dict['gen_opt'])
        trainer.discr_opt.load_state_dict(ckpt_dict['discr_opt'])

        return trainer