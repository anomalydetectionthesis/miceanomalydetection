import os
import torch

from dataset.dataset_builders.frame_prediction_dataset import FramePredictionDataset
from dataset.frame.thermal_frame import ThermalFrame
from frame_prediction.engine.train.train_engine import TrainEngine
from frame_prediction.engine.train.train_progress import TrainProgressPrinter
from frame_prediction.engine.train.trainer import AdvTrainer
from frame_prediction.modules.adversarial_model import AdversarialModel

from paths import ROOT_PATH


def resume_train_engine(train_ckpt_path, device = torch.device('cpu')):
    assert os.path.exists(train_ckpt_path)
    train_ckpt_dict = torch.load(train_ckpt_path, map_location=torch.device('cpu'))
    return TrainEngine.resume_fit(train_ckpt_dict, device)

def new_train_engine(n_img_channels, n_stacked_frames, gen_lr, discr_lr, int_weight=1,grad_weight=1, adv_weight=1, device = torch.device('cpu')):

    adv_model = AdversarialModel(img_channels=n_img_channels, n_stacked_frames=n_stacked_frames, device=device)
    trainer = AdvTrainer(
        adv_model,
        gen_lr=gen_lr,
        discr_lr=discr_lr,
        intensity_weight=int_weight,
        gradient_weight=grad_weight,
        adversarial_weight=adv_weight)
    train_engine = TrainEngine(trainer)
    return train_engine

def train_mice(epochs, batch_size, n_stacked_frames, gen_lr, discr_lr, dataset_path, ckpt_dir, verbose=True, verbose_freq=10000, device = torch.device('cpu')):
    progress_printer = TrainProgressPrinter(verbose, verbose_freq)

    #train_ds = FramePredictionDataset(dataset_path, ThermalFrame, n_stacked_frames)
    train_ds = FramePredictionDataset.from_hdf(dataset_path, n_stacked_frames, ThermalFrame.from_numpy, indexing_mode='train')
    train_eng = new_train_engine(1, n_stacked_frames, gen_lr, discr_lr, device=device)
    train_eng.fit(epochs, train_ds, batch_size, progress_printer = progress_printer, ckpt_dir=ckpt_dir)

def resume_mice_train(epochs, batch_size, train_ckpt_path, dataset_path, ckpt_dir, verbose=True, verbose_freq=10000, device = torch.device('cpu')):

    progress_printer = TrainProgressPrinter(verbose, verbose_freq)

    train_eng = resume_train_engine(train_ckpt_path, device=device)
    n_stacked_frames = train_eng.trainer.adv_model.n_stacked_frames

    train_ds = FramePredictionDataset.from_hdf(dataset_path, n_stacked_frames, ThermalFrame.from_numpy, indexing_mode='train')

    train_eng.fit(epochs, train_ds, batch_size, progress_printer=progress_printer, ckpt_dir=ckpt_dir)


if __name__ == '__main__':
    dataset_path = os.path.join(ROOT_PATH, 'data/mice/result_extractor/PUP-4_wt_2020_02_14_-045_09_31_12_000.h5')

    train_mice(
        epochs=2,
        batch_size=20,
        n_stacked_frames=10,
        gen_lr=1e-4,
        discr_lr=1e-5,
        dataset_path='/home/edo/Documents/Tesi/MiceAnomalyDetection/data/mice/dataset/wt/2020_02_12_wt_pnd1.h5',
        ckpt_dir=None,
        verbose=True,
        verbose_freq=1,
        device=torch.device('cpu'))