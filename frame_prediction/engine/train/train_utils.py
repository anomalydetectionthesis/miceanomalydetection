import json

import random

import numpy as np
import torch

from paths import ROOT_PATH
from plot_utils import loss_plot

def initialize_seed():
    seed = 30901681

    random.seed(seed)
    np.random.seed(seed)
    # fix the seed and make cudnn deterministic
    torch.manual_seed(seed)
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False
    torch.cuda.manual_seed_all(seed)

def extract_loss_stats(loss_dict_vector, stat_name):
    return [float(loss_dict[stat_name]) for loss_dict in loss_dict_vector]

def show_train_stats(train_stats: dict):
    #print(train_stats)
    fit_summary = train_stats.get('fit_summary')

    print(json.dumps(fit_summary, sort_keys = True, indent = 4))
    gen_loss_dict = fit_summary.get('gen_loss')
    discr_loss_dict = fit_summary.get('discr_loss')

    gen_loss = extract_loss_stats(gen_loss_dict, 'gen_loss')
    int_loss = extract_loss_stats(gen_loss_dict, 'int_loss')
    grad_loss = extract_loss_stats(gen_loss_dict, 'grad_loss')
    discr_loss = extract_loss_stats(discr_loss_dict, 'discr_loss')

    loss_plot(gen_loss, discr_loss)
    loss_plot(int_loss, grad_loss)


if __name__ == '__main__':
    #train_stats_file = os.path.join(ROOT_PATH, 'ckpt/mice/lr_g5e-5_d1e-6__loss_1i_1g_8a/12_train_ckpt.pth')
    train_ckpt_path = '/home/edo/Desktop/mice_ckpt/20_train_ckpt.pth'
    train_stats = torch.load(train_ckpt_path, map_location=torch.device('cpu'))

    show_train_stats(train_stats)