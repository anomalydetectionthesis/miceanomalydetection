import os

import torch

from dataset.Hdf5ResultsStore import Hdf5ResultsStore
from dataset.dataset_builders.frame_prediction_dataset import FramePredictionDataset
from dataset.frame.thermal_frame import ThermalFrame
from frame_prediction.engine.result_extractor.result_extractor_engine import ResultExtractor
from frame_prediction.modules.adversarial_model import AdversarialModel


def extract_mice_results(dataset_path: str, ckpt_path, prediction_results_file_path = None, latent_results_file_path = None, device = torch.device('cpu')):
    assert os.path.exists(dataset_path)
    assert os.path.exists(ckpt_path)
    #assert prediction_results_file_path is None or os.path.exists(prediction_results_file_path)
    #assert latent_results_file_path is None or os.path.exists(latent_results_file_path)

    latent_results_store = None if latent_results_file_path is None else Hdf5ResultsStore(latent_results_file_path)
    prediction_results_store = None if prediction_results_file_path is None else Hdf5ResultsStore(prediction_results_file_path)

    adv_model = AdversarialModel.from_ckpt(torch.load(ckpt_path, map_location=torch.device('cpu')), device)

    tester = ResultExtractor(adv_model)
    dataset = FramePredictionDataset.from_hdf(dataset_path, adv_model.n_stacked_frames, ThermalFrame.from_numpy)

    tester.extract_results(dataset, prediction_results_store, latent_results_store, ThermalFrame.from_model_tensor)


if __name__ == '__main__':
    from paths import ROOT_PATH
    env_path = os.path.join(ROOT_PATH, 'data/mice/frame_prediction/sf10_glr1e-4_dlr1e-5_loss_i1_w1_a1')
    ckpt_path = os.path.join(env_path, 'ckpt/25_model_ckpt.pth')

    prediction_results = '/home/edo/Desktop/predictions.h5'
    latent_results = '/home/edo/Desktop/latent.h5'
    dataset_path = '/home/edo/Documents/Tesi/MiceAnomalyDetection/data/mice/dataset/wt/2020_02_12_wt_pnd1.h5'

    extract_mice_results(
        dataset_path=dataset_path,
        ckpt_path=ckpt_path,
        prediction_results_file_path=prediction_results,
        latent_results_file_path=latent_results,
        device=torch.device('cpu')
    )