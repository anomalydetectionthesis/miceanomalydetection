from typing import Callable

import numpy as np
import torch

from torch.utils.data import DataLoader

from dataset.Hdf5ResultsStore import Hdf5ResultsStore
from dataset.dataset_builders.clustering_dataset import ClusteringDataset
from dataset.dataset_builders.predictions_dataset import PredictionsDataset
from dataset.frame.frame import Frame
from dataset.base.dataset_model import BaseVideo, BaseDataset
from frame_prediction.modules.downsample_model import DownsampleModel

from frame_prediction.modules.adversarial_model import AdversarialModel

from progress_bar import Progress
from result_evaluation.anomaly_score import compute_psnr


class ResultExtractor:
    def __init__(self, adv_model: AdversarialModel):
        self.adv_model = adv_model
        self.latent_model = DownsampleModel(adv_model.gen_net, adv_model.device)

    @torch.no_grad()
    def _extract_latent_results(self, video: BaseVideo):
        dl = DataLoader(video, batch_size=1)

        avg_pool_lantent_vecs = []
        max_pool_lantent_vecs = []

        for past_frames, next_frame in Progress(dl):
            latent_vec_batch: torch.Tensor = self.latent_model.predict(past_frames)
            latent_vec = latent_vec_batch[0]  # remove the batch dimension

            avg_pool_lantent_vecs.append(np.mean(latent_vec.numpy(), axis=(1, 2)))
            max_pool_lantent_vecs.append(np.max(latent_vec.numpy(), axis=(1, 2)))

        return avg_pool_lantent_vecs, max_pool_lantent_vecs

    @torch.no_grad()
    def _extract_prediction_results(self, video: BaseVideo, model_tensor_to_frame_fn: Callable[[torch.Tensor], Frame]):
        dl = DataLoader(video, batch_size=1)

        gt_frames = []
        pred_frames = []
        psnr_record = []

        for past_frames, next_frame in Progress(dl):

            pred_frame = self.adv_model.predict(past_frames)

            next_frame = model_tensor_to_frame_fn(next_frame[0])
            pred_frame = model_tensor_to_frame_fn(pred_frame[0])

            psnr = compute_psnr(pred_frame.to_tensor(), next_frame.to_tensor())

            gt_frames.append(next_frame)
            pred_frames.append(pred_frame)
            psnr_record.append(psnr)

        return gt_frames, pred_frames, psnr_record

    def extract_results(
            self,
            dataset: BaseDataset,
            prediction_results_store: Hdf5ResultsStore=None,
            latent_results_store:Hdf5ResultsStore=None,
            model_tensor_to_frame_fn: Callable[[torch.Tensor], Frame] = None
    ):

        for i in range(len(dataset)):
            video:BaseVideo = dataset[i]
            video_name = video.video_name
            print(f'[{i}/{len(dataset)}] -> {video_name}')

            if prediction_results_store is not None:
                gt_frame, pred_frame, psnr_record = self._extract_prediction_results(video, model_tensor_to_frame_fn)

                prediction_results_store.store_results(video_name, PredictionsDataset.GT_FRAME, np.array([frame.to_numpy() for frame in gt_frame]))
                prediction_results_store.store_results(video_name, PredictionsDataset.PRED_FRAME, np.array([frame.to_numpy() for frame in pred_frame]))
                prediction_results_store.store_results(video_name, PredictionsDataset.PSNR_VECTOR, np.array([psnr.numpy() for psnr in psnr_record]))

            if latent_results_store is not None:
                avg_pool_latent_vecs, max_pool_latent_vecs = self._extract_latent_results(video)

                latent_results_store.store_results(video_name, ClusteringDataset.AVG_POOL_LATENT_VECTOR, np.array(avg_pool_latent_vecs))
                latent_results_store.store_results(video_name, ClusteringDataset.MAX_POOL_LATENT_VECTOR, np.array(max_pool_latent_vecs))
