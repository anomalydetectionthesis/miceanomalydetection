
def video_info(video_name):
    grid_info, mouse_info = video_name.split('__')[0:2]

    strain, cage, date, date_id, = grid_info.split('_')
    mouse_label, geno, pnd, sex = mouse_info.split('_')

    pnd = int(pnd.replace('pnd', ''))
    mouse_class = 'normal' if 'wt' in geno else 'abnormal'
    mouse_id = f'{cage}_{mouse_label}'
    full_geno = f'{strain}_{geno}'

    return {
        'mouse_id': mouse_id,
        'mouse_class': mouse_class,
        'cage': cage,
        'geno': full_geno,
        'pnd': pnd,
        'sex': sex
    }
