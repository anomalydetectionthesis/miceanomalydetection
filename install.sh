#!/usr/bin/env bash

pip3 install torch torchvision torchsummary
pip3 install numpy
pip3 install matplotlib
pip3 install h5py
pip3 install scikit-learn
pip3 install opencv-python opencv-contrib-python
pip3 install toml

# NVIDIA Flownet2 dependencies
# pip3 install setproctitle scipy==1.1.0 scikit-image pillow