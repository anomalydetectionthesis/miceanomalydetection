import matplotlib.pyplot as plt

from dataset.frame.frame import Frame


def show_img(img: Frame, cmap=None):
    fig = plt.figure()

    plt.imshow(img.to_rgb_image(), cmap=cmap)
    plt.axis('off')

    plt.show()
    plt.close(fig)

def img_compare(target_img: Frame, gen_img: Frame, cmap=None):
    fig = plt.figure(figsize=(7, 3.5))

    a = fig.add_subplot(1, 2, 1)
    a.set_title('Target_img')
    imgplot = plt.imshow(target_img.to_rgb_image(), cmap=cmap)
    plt.axis('off')

    a = fig.add_subplot(1, 2, 2)
    a.set_title('Gen_img')
    imgplot = plt.imshow(gen_img.to_rgb_image(), cmap=cmap)
    plt.axis('off')

    fig.tight_layout()
    plt.show()

def loss_plot(gen_loss, discr_loss):
    fig, ax1 = plt.subplots()

    ax1_color = 'tab:red'
    ax1.plot(range(0, len(gen_loss)), gen_loss, color=ax1_color)
    ax1.set_ylabel("gen_loss", color=ax1_color)
    ax1.tick_params(axis='y', labelcolor=ax1_color)

    ax2= ax1.twinx()
    ax2_color = 'tab:blue'

    ax2.plot(range(0, len(discr_loss)), discr_loss, color=ax2_color)
    ax2.set_ylabel("discr_loss", color=ax2_color)
    ax2.tick_params(axis='y', labelcolor=ax2_color)

    fig.tight_layout()
    plt.show()