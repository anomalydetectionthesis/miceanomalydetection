from typing import Callable

from dataset.frame.frame import Frame
from dataset.base.base_hdf_dataset import HdfDataset
from dataset.base.dataset_model import BaseDataset
from dataset.base.dataset_transform import DatasetTransform
import numpy as np


class FrameTransform(DatasetTransform):
    def __init__(self, numpy_to_frame_fn: Callable[[np.ndarray], Frame]):
        self.numpy_to_frame_fn = numpy_to_frame_fn

    def get_video_len(self, data: np.ndarray):
        return len(data)

    def get_data(self, data: np.ndarray, index, **kwargs):
        return self.numpy_to_frame_fn(data[index])


class FrameDataset:

    @staticmethod
    def _validate_indexing_mode(indexing_mode):
        if indexing_mode is not None:
            assert indexing_mode in ['default']

    @staticmethod
    def from_hdf(dataset_path, numpy_to_frame_fn: Callable[[np.ndarray], Frame], indexing_mode=None) -> BaseDataset:
        FrameDataset._validate_indexing_mode(indexing_mode)
        transform = FrameTransform(numpy_to_frame_fn)
        hdf_dataset = HdfDataset.from_file(dataset_path, transform, indexing_mode)
        return hdf_dataset