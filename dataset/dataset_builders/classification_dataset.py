from typing import Callable
import torch
import os

from dataset.frame.frame import Frame
from dataset.frame.thermal_frame import ThermalFrame
from dataset.base.base_hdf_dataset import HdfDataset
from dataset.base.dataset_model import BaseDataset
from dataset.base.dataset_transform import DatasetTransform
import numpy as np


'''
The ClassificationDataset must be used with the classification model.
If n_past_frames = t, this dataset returns:
    - stacket_frames_model_tensor: the concatenation of n_past_frames Tensor([n_past_frames, n_channels, frame_height, frame_width])
    - targhet_value: Tensor() 1 = normal_frames, 0 = abnormal_frames
'''

class ClassificationTransform(DatasetTransform):
    def __init__(self, n_stacked_frames, numpy_to_frame_fn: Callable[[np.ndarray], Frame]):
        self.n_stacked_frames = n_stacked_frames
        self.numpy_to_frame_fn = numpy_to_frame_fn

    def get_video_len(self, data: np.ndarray):
        return len(data) - self.n_stacked_frames + 1

    def get_data(self, data: np.ndarray, index, video_name=None):
        assert video_name is not None

        target_value = [1] if 'wt' in video_name else [0]
        target_value = torch.tensor(target_value, dtype=torch.float32)

        stacked_frames = data[index:index + self.n_stacked_frames]
        stacked_frames = [self.numpy_to_frame_fn(np_frame).to_model_tensor() for np_frame in stacked_frames]

        stacked_frames = torch.cat(stacked_frames)
        return stacked_frames, target_value


class ClassificationDataset:


    @staticmethod
    def _validate_indexing_mode(indexing_mode):
        if indexing_mode is not None:
            assert indexing_mode in ['default', 'train']

    @staticmethod
    def from_hdf(dataset_path, n_stacked_frames, numpy_to_frame_fn: Callable[[np.ndarray], Frame], indexing_mode=None) -> BaseDataset:
        ClassificationDataset._validate_indexing_mode(indexing_mode)
        transform = ClassificationTransform(n_stacked_frames, numpy_to_frame_fn)
        hdf_dataset = HdfDataset.from_file(dataset_path, transform, indexing_mode)
        return hdf_dataset


if __name__ == '__main__':
    from torch.utils.data import DataLoader
    from paths import ROOT_PATH
    #dataset_path = os.path.join(ROOT_PATH, 'data/mice/frame_prediction/sf10_glr1e-4_dlr1e-5_loss_i1_w1_a1/latent_results/wt_2020_02_12_pnd1.h5')
    dataset_path = os.path.join(ROOT_PATH, 'data/mice/dataset/cdkl/cdkl_2020_02_12_pnd2.h5')

    ds = ClassificationDataset.from_hdf(dataset_path, n_stacked_frames=10, numpy_to_frame_fn=ThermalFrame.from_numpy, indexing_mode='train')
    in_tensor, target_tensor = ds[0]
    print(in_tensor.shape, target_tensor)
    print(len(ds))

    dl = DataLoader(ds, batch_size=20, shuffle=True)
    print(len(dl))
    for in_tensor, target_tensor in dl:
        print(in_tensor.shape, target_tensor.shape)
        break