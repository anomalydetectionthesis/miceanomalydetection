import os
from typing import Callable

import torch
from torch.utils.data import DataLoader

from dataset.frame.frame import Frame
from dataset.frame.thermal_frame import ThermalFrame
from dataset.base.base_hdf_dataset import HdfDataset
from dataset.base.dataset_model import BaseDataset
from dataset.base.dataset_transform import DatasetTransform
import numpy as np

from paths import ROOT_PATH


class FramePredictionTransform(DatasetTransform):
    def __init__(self, n_past_frames, numpy_to_frame_fn: Callable[[np.ndarray], Frame]):
        self.n_stacked_frames = n_past_frames + 1
        self.numpy_to_frame_fn = numpy_to_frame_fn

    def get_video_len(self, data: np.ndarray):
        return len(data) - self.n_stacked_frames + 1

    def get_data(self, data: np.ndarray, index, **kwargs):
        frames = data[index:index + self.n_stacked_frames]
        frames = [self.numpy_to_frame_fn(np_frame).to_model_tensor() for np_frame in frames]
        past_frames = torch.cat(frames[0:-1])
        next_frame = frames[-1]
        return past_frames, next_frame


class FramePredictionDataset:

    @staticmethod
    def from_hdf(
            dataset_path,
            n_past_frames,
            numpy_to_frame_fn: Callable[[np.ndarray],Frame],
            indexing_mode=None) -> BaseDataset:

        transform = FramePredictionTransform(n_past_frames, numpy_to_frame_fn)
        hdf_dataset = HdfDataset.from_file(dataset_path, transform, indexing_mode)
        return hdf_dataset


if __name__ == '__main__':
    dataset_path = os.path.join(ROOT_PATH, 'data/mice/dataset/wt/2020_02_12_wt_pnd1.h5')

    ds = FramePredictionDataset.from_hdf(dataset_path, n_past_frames=10, numpy_to_frame_fn=ThermalFrame.from_numpy, indexing_mode='train')
    in_tensor, target_tensor = ds[0]
    print(in_tensor.shape, target_tensor.shape)
    print(len(ds))

    dl = DataLoader(ds, batch_size=20, shuffle=True)
    print(len(dl))
    for in_tensor, target_tensor in dl:
        print(in_tensor.shape, target_tensor.shape)
        break
    # video: BaseVideo = ds[0]
    # print(video.video_name)