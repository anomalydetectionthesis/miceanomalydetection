from dataset.base.base_hdf_dataset import HdfDataset
from dataset.base.dataset_model import BaseDataset
from dataset.base.dataset_transform import DatasetTransform
import numpy as np
from result_evaluation.anomaly_score import anomaly_score


class AnomalyScoreTransform(DatasetTransform):

    def get_data(self, data: dict, index, **kwargs):
        score_vect = anomaly_score(data[AnomalyScoreDataset.PSNR_VECTOR])
        return score_vect[index]

    def get_video_len(self, data: np.ndarray):
        return len(data[AnomalyScoreDataset.PSNR_VECTOR])


class AnomalyScoreDataset:

    PSNR_VECTOR = 'psnr_record'

    @staticmethod
    def _validate_indexing_mode(indexing_mode):
        if indexing_mode is not None:
            assert indexing_mode in ['default']

    @staticmethod
    def from_hdf(dataset_path, indexing_mode=None) -> BaseDataset:
        AnomalyScoreDataset._validate_indexing_mode(indexing_mode)
        transform = AnomalyScoreTransform()
        hdf_dataset = HdfDataset.from_file(dataset_path, transform, indexing_mode)
        return hdf_dataset