import os

from dataset.base.base_hdf_dataset import HdfDataset
from dataset.base.dataset_model import BaseDataset
from dataset.base.dataset_transform import DatasetTransform
import numpy as np


class ClassificationResultsTransform(DatasetTransform):

    def get_video_len(self, data: np.ndarray):
        return len(data)

    def get_data(self, data: dict, field_name, **kwargs):
        return np.array(data[field_name])


class ClassificationResultsDataset:

    # dataset fields
    P_NORMALITY_VECTOR = 'p_norm_vect'

    @staticmethod
    def _validate_indexing_mode(indexing_mode):
        if indexing_mode is not None:
            assert indexing_mode in ['default']

    @staticmethod
    def from_hdf(dataset_path, indexing_mode=None) -> BaseDataset:
        ClassificationResultsDataset._validate_indexing_mode(indexing_mode)
        transform = ClassificationResultsTransform()
        hdf_dataset = HdfDataset.from_file(dataset_path, transform, indexing_mode)
        return hdf_dataset


if __name__ == '__main__':
    from paths import ROOT_PATH
    #dataset_path = os.path.join(ROOT_PATH, 'data/mice/frame_prediction/sf10_glr1e-4_dlr1e-5_loss_i1_w1_a1/latent_results/wt_2020_02_12_pnd1.h5')
    dataset_path = os.path.join(ROOT_PATH, 'data/mice/dataset/cdkl/cdkl_2020_02_12_pnd2.h5')

    ds = ClassificationResultsDataset.from_hdf(dataset_path, indexing_mode='train')
    in_tensor = ds[ClassificationResultsDataset.P_NORMALITY_VECTOR]
    print(in_tensor.shape)
    print(len(ds))