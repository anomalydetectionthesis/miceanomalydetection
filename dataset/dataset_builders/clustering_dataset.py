import os

from dataset.base.base_hdf_dataset import HdfDataset
from dataset.base.dataset_model import BaseDataset
from dataset.base.dataset_transform import DatasetTransform
import numpy as np

from paths import ROOT_PATH


class ClusteringDatasetTransform(DatasetTransform):

    def __init__(self, latent_field):
        self.latent_field = latent_field

    def _get_latent_vect(self, data: dict):
        return np.array(data[self.latent_field])

    def get_video_len(self, data: dict):
        latent_vect = self._get_latent_vect(data)
        return len(latent_vect)

    def get_data(self, data: dict, index, **kwargs):
        latent_vect = self._get_latent_vect(data)
        return latent_vect[index]


class ClusteringDataset:

    # Clustering dataset fields
    AVG_POOL_LATENT_VECTOR = 'avg_pool_latent_vect'
    MAX_POOL_LATENT_VECTOR = 'max_pool_latent_vect'

    @staticmethod
    def _validate_indexing_mode(indexing_mode):
        if indexing_mode is not None:
            assert indexing_mode in ['default']

    @staticmethod
    def from_hdf(dataset_path, indexing_mode=None, pool='avg', video_list = None) -> BaseDataset:
        """
        Args:
        - dataset_path [str]: Path to the hdf dataset file or path to folder
        - indexing_mode [str]: one of ['default']
        """
        ClusteringDataset._validate_indexing_mode(indexing_mode)
        assert pool in ['avg', 'max']
        if pool == 'avg':
            latent_field = ClusteringDataset.AVG_POOL_LATENT_VECTOR
        else:
            latent_field = ClusteringDataset.MAX_POOL_LATENT_VECTOR

        transform = ClusteringDatasetTransform(latent_field)
        return HdfDataset.from_file(dataset_path, transform, indexing_mode, video_list=video_list)


if __name__ == '__main__':
    #dataset_path = os.path.join(ROOT_PATH, 'data/mice/frame_prediction/sf10_glr1e-4_dlr1e-5_loss_i1_w1_a1/latent_results/wt_2020_02_12_pnd1.h5')
    dataset_path = os.path.join(ROOT_PATH,'data/mice/frame_prediction/sf10_glr1e-4_dlr1e-5_loss_i1_w1_a1/latent_results')

    ds = ClusteringDataset.from_hdf(dataset_path, indexing_mode=None, pool='avg')
    print(len(ds))
    #latent_data = np.concatenate([video[ClusteringDataset.LATENT_VECTOR] for video in Progress(ds)])
    #print(latent_data.shape)