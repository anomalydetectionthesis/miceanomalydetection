from typing import Callable

from dataset.frame.frame import Frame
from dataset.base.base_hdf_dataset import HdfDataset
from dataset.base.dataset_model import BaseDataset
from dataset.base.dataset_transform import DatasetTransform
import numpy as np
from result_evaluation.anomaly_score import anomaly_score


class PredictionsTransform(DatasetTransform):
    def __init__(self, numpy_to_frame_fn: Callable[[np.ndarray], Frame]):
        self.numpy_to_frame_fn = numpy_to_frame_fn

    def get_data(self, data: dict, index, **kwargs):
        score_vect = anomaly_score(data[PredictionsDataset.PSNR_VECTOR])

        gt_frame = self.numpy_to_frame_fn(data[PredictionsDataset.GT_FRAME][index])
        pred_frame = self.numpy_to_frame_fn(data[PredictionsDataset.PRED_FRAME][index])
        return gt_frame, pred_frame, score_vect[:index+1]

    def get_video_len(self, data: np.ndarray):
        return len(data[PredictionsDataset.PSNR_VECTOR])


class PredictionsDataset:
    GT_FRAME = 'gt_frames'
    PRED_FRAME = 'pred_frames'
    PSNR_VECTOR = 'psnr_record'

    @staticmethod
    def _validate_indexing_mode(indexing_mode):
        if indexing_mode is not None:
            assert indexing_mode in ['default']

    @staticmethod
    def from_hdf(dataset_path, numpy_to_frame_fn: Callable[[np.ndarray], Frame], indexing_mode=None) -> BaseDataset:
        PredictionsDataset._validate_indexing_mode(indexing_mode)
        transform = PredictionsTransform(numpy_to_frame_fn)
        hdf_dataset = HdfDataset.from_file(dataset_path, transform, indexing_mode)
        return hdf_dataset