import os
from typing import Callable

from dataset.frame.frame import Frame
from dataset.frame.thermal_frame import ThermalFrame
from dataset.base.base_hdf_dataset import HdfDataset
from dataset.base.dataset_model import BaseDataset
from dataset.base.dataset_transform import DatasetTransform
import numpy as np

from paths import ROOT_PATH


class VideoFragmentTransform(DatasetTransform):
    def __init__(self, n_stacked_frames, numpy_to_frame_fn: Callable[[np.ndarray], Frame]):
        self.n_stacked_frames = n_stacked_frames
        self.numpy_to_frame_fn = numpy_to_frame_fn

    def get_video_len(self, data: np.ndarray):
        return len(data) - self.n_stacked_frames + 1

    def get_data(self, data: np.ndarray, index, **kwargs):
        frames = data[index:index + self.n_stacked_frames]
        frames = [self.numpy_to_frame_fn(np_frame) for np_frame in frames]
        return frames


class VideoFragmentDataset:

    @staticmethod
    def _validate_indexing_mode(indexing_mode):
        if indexing_mode is not None:
            assert indexing_mode in ['default']

    @staticmethod
    def from_hdf( dataset_path, n_stacked_frames, numpy_to_frame_fn: Callable[[np.ndarray],Frame], indexing_mode=None) -> BaseDataset:
        VideoFragmentDataset._validate_indexing_mode(indexing_mode)
        transform = VideoFragmentTransform(n_stacked_frames, numpy_to_frame_fn)
        hdf_dataset = HdfDataset.from_file(dataset_path, transform, indexing_mode)
        return hdf_dataset


if __name__ == '__main__':
    dataset_path = os.path.join(ROOT_PATH, 'data/mice/dataset/wt/2020_02_12_wt_pnd1.h5')
    ds = VideoFragmentDataset.from_hdf(dataset_path, n_stacked_frames=10, numpy_to_frame_fn=ThermalFrame.from_numpy)