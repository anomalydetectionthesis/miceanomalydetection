import os

import h5py
import numpy as np


class Hdf5ResultsStore:
    def __init__(self, dataset_path):
        self.dataset_path = dataset_path

    def keys(self):
        with h5py.File(self.dataset_path, "r") as dataset:
            keys = tuple(dataset.keys())
        return keys

    def store_results(self, video_name: str, result_name: str, result_data: np.ndarray, compress=False):
        compression = "gzip" if compress == True else None

        open_mode = "r+" if os.path.exists(self.dataset_path) else "w"

        with h5py.File(self.dataset_path, open_mode) as dataset:
            if video_name not in dataset.keys():
                dataset.create_group(video_name)

            video_results = dataset.get(video_name)
            video_results.create_dataset(result_name, shape=result_data.shape, dtype=result_data.dtype, data=result_data, compression=compression)