import numpy as np
import torch
from PIL import Image
from torchvision import transforms

import matplotlib.pyplot as plt

from dataset.frame.custom_transforms import UnNormalize
from dataset.frame.frame import Frame


class GrayscaleFrame(Frame):
    @staticmethod
    def from_numpy(np_array) -> 'Frame':
        return GrayscaleFrame(np_array)

    def __init__(self, np_frame: np.ndarray):
        if np_frame.shape != (256, 256):
            transform = transforms.Compose([
                transforms.Lambda(lambda frame: Image.fromarray(frame)),
                transforms.Resize(size=(256, 256)),
                transforms.Lambda(lambda frame: np.array(frame, dtype=np.uint8))
            ])
            np_frame = transform(np_frame)

        self.frame = np_frame

    @staticmethod
    def from_model_tensor(model_tensor) -> 'GrayscaleFrame':
        transform = transforms.Compose([
            UnNormalize(0.5, 0.5),
            transforms.ToPILImage(),
            transforms.Lambda(lambda frame: np.array(frame, dtype=np.uint8))
        ])
        frame = transform(model_tensor)
        return GrayscaleFrame(frame)

    @property
    def shape(self):
        return self.frame.shape

    def to_model_tensor(self) -> torch.FloatTensor:
        transform = transforms.Compose([
            transforms.ToTensor(),
            transforms.Normalize((0.5,), (0.5,))
        ])
        return transform(self.frame)

    def to_tensor(self) -> torch.FloatTensor:
        transform = transforms.Compose([
            transforms.ToTensor()
        ])
        return transform(self.frame)

    def to_numpy(self) -> np.ndarray:
        return self.frame

    def to_rgb_image(self, title=None, fig_size=None) -> np.ndarray:
        fig = plt.figure(figsize=fig_size)
        fig.add_subplot(111)

        plt.imshow(self.frame, cmap='gray')

        if title is not None:
            plt.title(title)

        fig.tight_layout(pad=0)
        fig.canvas.draw()
        # Save fig in a numpy array.
        rgb_string = fig.canvas.tostring_rgb()
        canvas_shape = fig.canvas.get_width_height()
        pil_image = Image.frombytes("RGB", canvas_shape, rgb_string)
        data = np.array(pil_image)
        plt.close(fig)

        return data
