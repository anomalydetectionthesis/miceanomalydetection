import numpy as np
import torch
from torchvision import transforms

import matplotlib.pyplot as plt
from PIL import Image

from dataset.frame.custom_transforms import UnNormalize
from dataset.frame.frame import Frame


class ThermalFrame(Frame):
    min_temp = 0
    max_temp = 60

    def __init__(self, np_frame: np.ndarray):
        self.frame = np_frame

    @staticmethod
    def from_numpy(np_array) -> 'Frame':
        return ThermalFrame(np_array)

    @staticmethod
    def from_model_tensor(model_tensor):
        transform = transforms.Compose([
            UnNormalize(0.5, 0.5),
            transforms.Lambda(lambda tensor: tensor * (ThermalFrame.max_temp - ThermalFrame.min_temp) + ThermalFrame.min_temp),
            transforms.Lambda(lambda frame: frame.numpy()[0])
        ])
        frame = transform(model_tensor)

        return ThermalFrame(frame)

    @property
    def shape(self):
        return self.frame.shape

    def to_model_tensor(self) -> torch.FloatTensor:
        '''
        Return: pixel values in [-1, 1] range
        '''
        transform = transforms.Compose([
            self._to_tensor_transform(),
            self._normalize()
        ])
        return transform(self.frame)

    def to_tensor(self) -> torch.FloatTensor:
        '''
            Return: pixel values in [-1, 1] range
        '''
        transform = self._to_tensor_transform()
        return transform(self.frame)

    def to_numpy(self):
        return self.frame


    def to_rgb_image(self, title=None, fig_size=None):
        fig = plt.figure(figsize=fig_size)
        fig.add_subplot(111)
        plt.imshow(self.frame, cmap='jet', clim=(17, 39))
        plt.colorbar()

        if title is not None:
            plt.title(title)

        fig.tight_layout(pad=0)
        fig.canvas.draw()

        # Save fig in a numpy array.
        rgb_string = fig.canvas.tostring_rgb()
        canvas_shape = fig.canvas.get_width_height()
        pil_image = Image.frombytes("RGB", canvas_shape, rgb_string)
        data = np.array(pil_image)
        plt.close(fig)

        return data


    def _to_tensor_transform(self):
        return transforms.Compose([
            transforms.Lambda(lambda np_img: (np_img - ThermalFrame.min_temp) / (ThermalFrame.max_temp - ThermalFrame.min_temp)),
            transforms.Lambda(lambda np_img: np.expand_dims(np_img, axis=0)),
            transforms.Lambda(lambda np_img: torch.from_numpy(np_img)),
        ])

    def _normalize(self):
        return transforms.Normalize((0.5,), (0.5,))


