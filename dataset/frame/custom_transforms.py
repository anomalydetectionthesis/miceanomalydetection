from torchvision import transforms


def UnNormalize(mean, std):
    '''
    Revert the transforms.Normalize(mean, std) operation
    '''
    return transforms.Lambda(lambda tensor_img: (tensor_img*std)+mean)