from abc import ABC, abstractmethod

import numpy as np
import torch


class Frame(ABC):

    @staticmethod
    @abstractmethod
    def from_model_tensor(model_tensor) -> 'Frame': pass

    @staticmethod
    @abstractmethod
    def from_numpy(np_array) -> 'Frame': pass

    @property
    @abstractmethod
    def shape(self): pass

    @abstractmethod
    def to_model_tensor(self) -> torch.FloatTensor: pass

    @abstractmethod
    def to_tensor(self) -> torch.FloatTensor: pass

    @abstractmethod
    def to_numpy(self) -> np.ndarray: pass

    @abstractmethod
    def to_rgb_image(self, title=None, fig_size=None) -> np.ndarray: pass