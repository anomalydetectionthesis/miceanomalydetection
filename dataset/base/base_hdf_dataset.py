import os
from abc import ABC
from typing import Dict

import h5py

from dataset.base.dataset_transform import DatasetTransform
from dataset.base.dataset_model import BaseDataset, BaseVideo


def _open_dataset(dataset_path):
    return h5py.File(dataset_path,'r')


class HdfVideo(BaseVideo):
    def __init__(self, dataset_path, video_name, dataset_transform: DatasetTransform):
        super().__init__(video_name)
        self.dataset_path = dataset_path
        self.video_name = video_name
        self.dataset_transform = dataset_transform
        self._len = None

    def _get_video_len(self, dataset):
        if self._len is None:
            self._len = self.dataset_transform.get_video_len(dataset[self.video_name])
        return self._len

    def _get_data(self, dataset, index):
        return self.dataset_transform.get_data(dataset[self.video_name], index, video_name=self.video_name)

    def __len__(self):
        if self._len is None:
            with _open_dataset(self.dataset_path) as dataset:
                self._len = self._get_video_len(dataset)

        return self._len

    def __getitem__(self, item):
        with _open_dataset(self.dataset_path) as dataset:
            return self._get_data(dataset, item)

    def __iter__(self):
        for index in range(self.__len__()):
            with _open_dataset(self.dataset_path) as dataset:
                data = self._get_data(dataset, index)
            yield data


class HdfDataset(BaseDataset, ABC):
    def __init__(self, videos: Dict[str, HdfVideo], indexing_mode=None):
        super().__init__(indexing_mode)

        self.videos = videos
        self._len = None

    @staticmethod
    def _get_dataset_videos(dataset_file_path, dataset_transform):
        videos = {}
        with _open_dataset(dataset_file_path) as dataset:
            for video_name in dataset.keys():
                videos[video_name] = HdfVideo(dataset_file_path, video_name, dataset_transform)

        return videos

    @staticmethod
    def from_file(dataset_path, dataset_transform, indexing_mode=None, video_list=None):
        '''

        :param dataset_path:
        :param dataset_transform:
        :param indexing_mode:
        :param video_list: usefull whit folder ds_path. List of videos to include in the dataset
        :return:
        '''
        assert os.path.exists(dataset_path)

        modes = {
            'train': HdfFullDatasetIndexing,
            'default': HdfFDefaultDatasetIndexing
        }
        HdfDatasetClass = modes.get(indexing_mode, HdfFDefaultDatasetIndexing)

        if os.path.isfile(dataset_path):
            dataset_files_paths = [dataset_path]
        elif os.path.isdir(dataset_path):
            h5_dataset_files = [file_name for file_name in os.listdir(dataset_path) if '.h5' in file_name]
            dataset_files_paths = [os.path.join(dataset_path,file_name) for file_name in h5_dataset_files]
        else:
            raise ValueError(f"Invalid dataset path: {dataset_path}")

        videos = {}
        for dataset_file in dataset_files_paths:
            file_videos = HdfDataset._get_dataset_videos(dataset_file, dataset_transform)
            # filter video list
            if video_list is not None:
                file_videos = {vid_name: vid_ds for vid_name, vid_ds in file_videos.items() if vid_name in video_list}

            keys_intersection = set(videos.keys())& set(file_videos.keys())
            assert len(keys_intersection) == 0
            videos = {
                **videos,
                **file_videos
            }

        return HdfDatasetClass(videos, indexing_mode)


class HdfFullDatasetIndexing(HdfDataset):

    def _get_video_from_ds_index(self, index):
        start_index = 0

        for video in self.videos.values():
            video_len = len(video)
            if index >= start_index + video_len:
                start_index += video_len
            else:
                video_index = index - start_index
                return video, video_index

    def __getitem__(self, item):
        video, video_index = self._get_video_from_ds_index(item)
        return video[video_index]

    def __iter__(self):
        with _open_dataset(self.dataset_path) as dataset:
            for video in self.videos.values():
                video_len = len(video)
                for index in range(video_len):
                    yield video._get_data(dataset, index)

    def __len__(self):
        if self._len is None:
            current_len = 0
            for video in self.videos.values():
                current_len += len(video)
            self._len = current_len

        return self._len


class HdfFDefaultDatasetIndexing(HdfDataset):

    def __getitem__(self, item):
        assert type(item) == int or type(item) == str

        if type(item) == int:
            item = tuple(self.videos.keys())[item]

        return self.videos.get(item)

    def __iter__(self):
        for video in self.videos.values():
            yield video

    def __len__(self):
        return len(tuple(self.videos.keys()))