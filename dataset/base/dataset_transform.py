from abc import ABC, abstractmethod

class DatasetTransform(ABC):
    @abstractmethod
    def get_video_len(self, data): pass

    @abstractmethod
    def get_data(self, data, index, **kwargs): pass
