import torch
from abc import ABC, abstractmethod

class BaseVideo(ABC, torch.utils.data.Dataset):
    def __init__(self, video_name):
        self.video_name = video_name

    @abstractmethod
    def __len__(self):
        pass

    @abstractmethod
    def __getitem__(self, item): pass

    @abstractmethod
    def __iter__(self): pass


class BaseDataset(ABC, torch.utils.data.Dataset):
    def __init__(self, indexing_mode: str = 'default'):
        indexing_mode = 'default' if indexing_mode is None else indexing_mode

        assert indexing_mode in ['default', 'train']

        self.indexing_mode = indexing_mode

    @abstractmethod
    def __len__(self):
        pass

    @abstractmethod
    def __getitem__(self, item):
        pass

    @abstractmethod
    def __iter__(self):
        pass