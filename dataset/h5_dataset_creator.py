import os
import h5py


def h5_dataset_merge(src_dataset_folder, dest_dataset_path, dataset_split:list):
    assert dataset_split is not None

    h5_files = [file_name for file_name in os.listdir(src_dataset_folder) if '.h5' in file_name]

    assert not os.path.exists(dest_dataset_path)

    merge_count = 0
    with h5py.File(os.path.join(dest_dataset_path), 'w') as dest_dataset:
        for src_file_name in h5_files:
            with h5py.File(os.path.join(src_dataset_folder, src_file_name), 'r') as src_dataset:
                for dataset_name, data in src_dataset.items():
                    if dataset_name in dataset_split:
                        merge_count+=1
                        print(f'[{merge_count}/{len(dataset_split)}]: merging {dataset_name}')
                        dest_dataset.create_dataset(dataset_name, shape=data.shape, dtype=data.dtype, data=data)

if __name__ == '__main__':
    h5_dataset_merge('/home/edo/Documents/Tesi/MiceAnomalyDetection/data/mice/dataset','/home/edo/Desktop/test.h5', ['wt_2020_02_17_f__zpdx_pnd6_m'])