import torch

from classifier.modules.classifier import MiceClassifier
from classifier.train_classifier import ClassifierTrainer
from dataset.dataset_builders.classification_dataset import ClassificationDataset

from dataset.frame.thermal_frame import ThermalFrame


def train_mice(epochs, batch_size, n_stacked_frames, lr, train_ds_path, val_ds_path, ckpt_dir, verbose=True, verbose_freq=10000, device = torch.device('cpu'), epoch_portions = 10):

    train_ds = ClassificationDataset.from_hdf(train_ds_path, n_stacked_frames, ThermalFrame.from_numpy, indexing_mode='train')
    validation_ds = ClassificationDataset.from_hdf(val_ds_path, n_stacked_frames, ThermalFrame.from_numpy, indexing_mode='train')

    model = MiceClassifier(in_channels=n_stacked_frames,device=device)
    train_eng = ClassifierTrainer(model, lr)
    train_eng.fit(train_ds, validation_ds, epochs, batch_size, ckpt_dir=ckpt_dir, epoch_portions=epoch_portions)


def resume_mice_train(epochs, batch_size, train_ckpt_path, train_ds_path, val_ds_path, ckpt_dir, verbose=True, verbose_freq=10000, device = torch.device('cpu'), epoch_portions = 10):
    train_ckpt_dict = torch.load(train_ckpt_path, map_location=torch.device('cpu'))
    train_eng = ClassifierTrainer.from_ckpt(train_ckpt_dict, device)

    n_stacked_frames = train_eng.model.in_channels

    train_ds = ClassificationDataset.from_hdf(train_ds_path, n_stacked_frames, ThermalFrame.from_numpy, indexing_mode='train')
    validation_ds = ClassificationDataset.from_hdf(val_ds_path, n_stacked_frames, ThermalFrame.from_numpy, indexing_mode='train')
    train_eng.fit(train_ds, validation_ds, epochs, batch_size, ckpt_dir=ckpt_dir, epoch_portions=epoch_portions)


if __name__ == '__main__':
    ds_path = '/home/edo/Documents/Tesi/MiceAnomalyDetection/data/mice/dataset/cdkl5_2020_02_12_3bis.h5'
    val_ds_path = '/home/edo/Documents/Tesi/MiceAnomalyDetection/data/mice/dataset/cdkl5_2020_02_13_3bis.h5'

    train_mice(
        epochs=1,
        batch_size=20,
        n_stacked_frames=10,
        lr =1e-4,
        train_ds_path=ds_path,
        val_ds_path=val_ds_path,
        ckpt_dir = None,
        verbose=True,
        verbose_freq=10000,
        device=torch.device('cpu'))