
import os
from time import time

import torch
import numpy as np
from torch import optim
from torch.nn import BCELoss
from torch.utils.data import DataLoader

from classifier.modules.classifier import MiceClassifier
from dataset.base.dataset_model import BaseDataset
from progress_bar import Progress


class EarlyStop:
    def __init__(self, patience=10):
        self.early_stop_patience = patience
        self.early_stop_count = 0
        self.last_val_loss = None
        self.min_val_loss = None

    def update(self, val_loss):
        if self.last_val_loss is None:  # initialize early stop
            self.min_val_loss= val_loss

        elif val_loss < self.min_val_loss:  # val loss improvement -> reset counter
            self.min_val_loss = val_loss
            self.early_stop_count = 0
        else:
            self.early_stop_count += 1

        self.last_val_loss = val_loss
        print(self.early_stop_count)

    def stop_train(self):
        return self.early_stop_count >= self.early_stop_patience


class ClassifierTrainer:
    def __init__(self, model: MiceClassifier, lr):
        self.current_epoch = 0
        self.train_loss_history = []
        self.validation_loss_history = []
        self.train_duration = 0
        self.model = model
        self.lr = lr
        self.opt = optim.Adam(params=model.parameters(), lr=lr)
        self.device = model.device
        self.early_stop = EarlyStop()

    @torch.no_grad()
    def validate(self, model, val_dl, bce_loss_fn):
        model.eval()

        bce_loss_sum = 0
        n_batch = len(val_dl)

        wt_probs = np.array([], dtype=np.float32)
        cdkl_probs = np.array([], dtype=np.float32)

        for input, target in Progress(val_dl):
            input = input.to(self.device)
            target = target.to(self.device)
            model_out = model(input)

            batch_loss = bce_loss_fn(model_out, target)
            bce_loss_sum += batch_loss.cpu().numpy()

            target = target.cpu().numpy()
            model_out = model_out.cpu().numpy()

            wt_probs = np.concatenate([wt_probs, model_out[target==1]])
            cdkl_probs = np.concatenate([cdkl_probs, model_out[target == 0]])


        l1_loss = (np.sum(1-wt_probs)+np.sum(cdkl_probs))/(len(wt_probs)+len(cdkl_probs))
        mean_bce_loss = bce_loss_sum/n_batch
        return mean_bce_loss, {'bce_loss':mean_bce_loss, 'l1_loss': l1_loss,'mean_wt_prob': np.mean(wt_probs), 'std_wt_probs': np.std(wt_probs), 'mean_cdkl_prob': np.mean(cdkl_probs), 'std_cdkl_probs': np.std(cdkl_probs)}

    def train_step(self, model, input, target, opt, loss_fn):

        input = input.to(self.device)
        target = target.to(self.device)
        model_out = model(input)

        loss = loss_fn(model_out,target)

        opt.zero_grad()
        loss.backward()
        opt.step()

        step_loss = loss.cpu().detach().numpy()

        return step_loss


    def fit(self, train_dataset: BaseDataset, validation_dataset: BaseDataset, epochs, batch_size, ckpt_dir, epoch_portions = 10 ):
        assert train_dataset.indexing_mode == 'train'
        assert validation_dataset.indexing_mode == 'train'

        model = self.model
        train_dl = DataLoader(train_dataset, batch_size=batch_size, shuffle=True)
        validation_dl = DataLoader(validation_dataset, batch_size=batch_size)
        opt = self.opt
        bce_loss = BCELoss(reduction='mean')

        portion_batches = len(train_dl)//epoch_portions

        for epoch in range(self.current_epoch + 1, epochs + 1):
            epoch_info = f'[{epoch}/{epochs}]'
            self.current_epoch = epoch
            print(epoch_info)
            model.train()
            e0 = time()

            portion_count = 0
            portion_batch_count = 0
            portion_loss_sum = 0

            for input, target in Progress(train_dl):
                step_loss = self.train_step(model, input, target, opt, bce_loss)
                portion_loss_sum += step_loss
                portion_batch_count += 1

                if portion_batch_count == portion_batches:
                    portion_count +=1
                    mean_portion_loss = portion_loss_sum/portion_batches
                    print('\nstart portion validation')
                    val_bce_loss, val_loss_dict = self.validate(model, validation_dl, bce_loss)

                    self.train_loss_history.append(mean_portion_loss)
                    self.validation_loss_history.append(val_loss_dict)
                    print(f'{epoch_info} train_loss: {mean_portion_loss} val_loss: {val_loss_dict}')
                    portion_batch_count = 0
                    portion_loss_sum = 0
                    self._save_portion_ckpt(ckpt_dir, portion_count)

                    self.early_stop.update(val_bce_loss)

                    if self.early_stop.stop_train():
                        print("Early Stop")
                        epoch_duration = time() - e0
                        self.train_duration += epoch_duration
                        print(f'{epoch_info} epoch_time: {epoch_duration}')
                        self._save_ckpt(ckpt_dir)
                        return

            epoch_duration = time() - e0
            self.train_duration += epoch_duration
            print(f'{epoch_info} epoch_time: {epoch_duration}')
            self._save_ckpt(ckpt_dir)

    def _fit_summary(self):
        return {
            'epoch': self.current_epoch,
            'train_time': self.train_duration,
            'train_loss': self.train_loss_history,
            'validation_loss': self.validation_loss_history,
        }

    def _save_portion_ckpt(self, ckpt_dir, portion_id):
        model_ckpt_path = os.path.join(ckpt_dir, f'{self.current_epoch}_{portion_id}_model_ckpt.pth')
        self.model.save_ckpt(model_ckpt_path)

    def _save_ckpt(self, ckpt_dir):
        if ckpt_dir is not None:
            model_ckpt_path = os.path.join(ckpt_dir, f'{self.current_epoch}_model_ckpt.pth')
            train_ckpt_path = os.path.join(ckpt_dir, f'{self.current_epoch}_train_ckpt.pth')

            train_status = {}
            train_status['fit_summary'] = self._fit_summary()
            train_status['opt_lr'] = self.lr
            train_status['opt_state_dict'] = self.opt.state_dict()
            train_status['model_state_dict'] = self.model.get_state_dict()

            # save model parameters
            self.model.save_ckpt(model_ckpt_path)
            torch.save(train_status, train_ckpt_path)

    @staticmethod
    def from_ckpt(ckpt_dict, device=torch.device('cpu')):
        model_ckpt_dict = ckpt_dict['model_state_dict']
        model = MiceClassifier.from_ckpt(model_ckpt_dict, device)

        trainer = ClassifierTrainer(model, ckpt_dict['opt_lr'])
        trainer.opt.load_state_dict(ckpt_dict['opt_state_dict'])

        fit_summary = ckpt_dict['fit_summary']
        trainer.current_epoch = fit_summary['epoch']
        trainer.train_loss_history = fit_summary['train_loss']
        trainer.validation_loss_history = fit_summary['validation_loss']
        trainer.train_duration = fit_summary['train_time']
        return trainer
