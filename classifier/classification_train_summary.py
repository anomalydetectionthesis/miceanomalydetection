import os
import matplotlib.pyplot as plt
import pandas as pd
import torch
from tabulate import tabulate


class ClassificationTrainSummary:
    def __init__(self, train_ckpt_dict):
        self.train_ckpt_dict = train_ckpt_dict
        self.fit_summary = self.train_ckpt_dict['fit_summary']

        self.epochs = self.fit_summary['epoch']
        self.train_duration = self.fit_summary['train_time']
        self.train_loss = self.fit_summary['train_loss']
        self.val_loss = self.fit_summary['validation_loss']

        assert len(self.train_loss) == len(self.val_loss)

        val_loss_dict = {k: [d[k] for d in self.val_loss] for k in self.val_loss[0].keys()}
        self.loss_df: pd.DataFrame = pd.DataFrame({
            'epoch': range(1, len(self.train_loss) + 1),
            'train_loss': self.train_loss,
            **val_loss_dict
        }).set_index('epoch')

    def show_summary(self):
        print(tabulate(self.loss_df, headers='keys', tablefmt="psql", floatfmt='.5f'))

    def plot_stats(self):
        fig, ax1 = plt.subplots()
        self.loss_df.plot(ax=ax1, y=['train_loss'], use_index=True)
        #self.loss_df.plot(ax=ax1, y=['bce_loss'], use_index=True, secondary_y=True)
        self.loss_df.plot(ax=ax1, y=['bce_loss'], use_index=True)
        plt.show()


if __name__ == '__main__':

    #train_ckpt_path = os.path.join(ROOT_PATH, 'data/mice/classification/1e-4/ckpt/10_train_ckpt.pth')
    train_ckpt_path = '/home/edo/Documents/Tesi/MiceAnomalyDetection/data/mice/classification/sf01_1e-5/1_train_ckpt.pth'
    ckpt_dict = torch.load(train_ckpt_path, map_location=torch.device('cpu'))

    summary = ClassificationTrainSummary(ckpt_dict)
    summary.show_summary()
    summary.plot_stats()
