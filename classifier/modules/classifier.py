import torch
from torch import nn
from torchsummary import summary

IN_CHANNELS = 'in_channels'
MODEL_STATE_DICT = 'model_state_dict'

class Flatten(nn.Module):
    def __init__(self):
        super(Flatten, self).__init__()

    def forward(self, x: torch.Tensor):
        flatten =x.view(x.size(0), -1)
        return flatten

class ConvClassificationLayer(nn.Module):
    def __init__(self, in_channels: int, out_channels: int, conv_kernel: int = 4, pool_kernel = 2, stride=1):
        super(ConvClassificationLayer, self).__init__()
        pad = (conv_kernel-1)//2

        self.net = nn.Sequential(
            nn.Conv2d(
                in_channels=in_channels,
                out_channels=out_channels,
                kernel_size=conv_kernel,
                stride=stride,
                padding=pad
            ),
            nn.BatchNorm2d(num_features=out_channels),
            nn.LeakyReLU(negative_slope=0.2),
            nn.MaxPool2d(kernel_size=pool_kernel),
        )

    def forward(self, input):
        return self.net(input)


class MiceClassifier(nn.Module):
    def __init__(self, in_channels: int, device = torch.device('cpu')):
        super(MiceClassifier, self).__init__()

        self.in_channels = in_channels

        out_channels = (64, 128, 256, 512)

        self.net = nn.Sequential(
            ConvClassificationLayer(in_channels, out_channels[0], stride=2),
            ConvClassificationLayer(out_channels[0], out_channels[1]),
            ConvClassificationLayer(out_channels[1], out_channels[2]),
            ConvClassificationLayer(out_channels[2], out_channels[3]),
            Flatten(),
            nn.Linear(in_features=2048, out_features=512),
            nn.LeakyReLU(negative_slope=0.2),
            nn.Linear(in_features=512, out_features=512),
            nn.LeakyReLU(negative_slope=0.2),
            nn.Linear(in_features=512, out_features=1),
            nn.Sigmoid()
        )
        self.device = device
        self.to(device)
        self.apply(MiceClassifier._init_weight)

    def forward(self, input):
        img_shape = input.shape[-2:]
        assert img_shape == (104,104)
        return self.net(input)

    @staticmethod
    def from_ckpt(ckpt_dict, device=torch.device('cpu')):
        model = MiceClassifier(ckpt_dict[IN_CHANNELS], device)
        model.load_state_dict(ckpt_dict[MODEL_STATE_DICT])
        return model

    def get_state_dict(self):
        state = {}
        state[IN_CHANNELS] = self.in_channels
        state[MODEL_STATE_DICT] = self.state_dict()
        return state

    def save_ckpt(self, checkpoint_path):
        torch.save(self.get_state_dict(), checkpoint_path)

    @staticmethod
    def _init_weight(module):
        #print(f"init weight {type(module)}")
        if type(module) == nn.Linear:
            # torch.nn.init.xavier_normal()
            module.bias.data.fill_(0.01)
            nn.init.kaiming_normal_(module.weight, a=0.2)
        elif type(module) == nn.Conv2d:
            module.bias.data.fill_(0.01)
            nn.init.kaiming_normal_(module.weight, a=0.2)
        # else:
        #     nn.init.xavier_normal(module.weight)



if __name__ == '__main__':
    model = MiceClassifier(10)
    summary(model, (10, 104, 104), batch_size=1)