import os

import torch
import numpy as np
from torch.utils.data import DataLoader

from classifier.modules.classifier import MiceClassifier
from dataset.Hdf5ResultsStore import Hdf5ResultsStore
from dataset.dataset_builders.classification_dataset import ClassificationDataset
from dataset.dataset_builders.classification_results_dataset import ClassificationResultsDataset
from dataset.frame.thermal_frame import ThermalFrame

from dataset.base.dataset_model import BaseDataset, BaseVideo

from progress_bar import Progress


class ResultExtractor:
    def __init__(self, model: MiceClassifier):
        self.model = model
        self.device = model.device

    @torch.no_grad()
    def classify_video(self, video: BaseVideo):
        self.model.eval()

        dl = DataLoader(video, batch_size=1)
        p_norm_vect = []
        for input, target in Progress(dl):
            input = input.to(self.device)
            target = target.to(self.device)
            pred = float(self.model(input).cpu().numpy()[0])
            p_norm_vect.append(pred)

        return np.array(p_norm_vect)

    def extract_results(self, dataset: BaseDataset, results_store: Hdf5ResultsStore):

        for i in range(len(dataset)):
            video: BaseVideo = dataset[i]

            video_name = video.video_name
            print(f'[{i}/{len(dataset)}] -> {video_name}')

            p_norm_vect = self.classify_video(video)
            results_store.store_results(video_name, ClassificationResultsDataset.P_NORMALITY_VECTOR, p_norm_vect)


def extract_results(dataset_path, ckpt_path, results_file_path, device):
    assert os.path.exists(ckpt_path)

    ckpt_dict = torch.load(ckpt_path, map_location=torch.device('cpu'))

    model = MiceClassifier.from_ckpt(ckpt_dict=ckpt_dict, device=device)
    extractor = ResultExtractor(model)
    n_stacked_frames = model.in_channels

    dataset = ClassificationDataset.from_hdf(dataset_path, n_stacked_frames, ThermalFrame.from_numpy)
    results_store = Hdf5ResultsStore(results_file_path)
    extractor.extract_results(dataset, results_store)

if __name__ == '__main__':
    ckpt_path = '/home/edo/Documents/Tesi/MiceAnomalyDetection/data/mice/classification/ckpt/10_model_ckpt.pth'
    dataset_path = '/data/mice/dataset/cdkl/cdkl_2020_02_12_pnd2.h5'
    result_path = '/home/edo/Desktop/result-test.h5'

    extract_results(dataset_path, ckpt_path, result_path, torch.device('cpu'))