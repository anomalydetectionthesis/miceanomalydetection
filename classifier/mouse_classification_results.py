import os

import pandas as pd
import numpy as np
from tabulate import tabulate

from dataset.dataset_builders.classification_results_dataset import ClassificationResultsDataset
from paths import ROOT_PATH
from result_evaluation.stats_utils import compute_auc


def compute_accuracy(p_norm_vect: np.array, target_class_value):
    p_norm_vect = p_norm_vect.round()
    target_vect = np.ones_like(p_norm_vect)*target_class_value

    return np.array(p_norm_vect == target_vect).sum()/len(target_vect)


def mouse_video_name(full_video_name: str):
    splitted_name = full_video_name.split('__')
    mouse_group_name = splitted_name[0]
    mouse_name = splitted_name[1]
    return mouse_group_name, mouse_name


if __name__ == '__main__':
    results_folder = os.path.join(ROOT_PATH, 'data/mice/classification/sf01_1e-5/classification_results')
    results_dataset = ClassificationResultsDataset.from_hdf(results_folder)

    mouse_group_list = []
    mouse_name_list = []
    p_normality_list = []

    for video in results_dataset:
        mouse_group_name, mouse_name = mouse_video_name(video.video_name)
        mouse_group_list.append(mouse_group_name)
        mouse_name_list.append(mouse_name)
        p_normality_list.append(video[ClassificationResultsDataset.P_NORMALITY_VECTOR])

    pd_result = pd.DataFrame({
        "mouse_group": mouse_group_list,
        "mouse_name": mouse_name_list,
    })

    pd_result['is_normal'] = pd_result['mouse_group'].apply(lambda mouse_group: 'wt' in mouse_group)
    pd_result['mean_prob'] = pd.Series([np.mean(p_vect) for p_vect in p_normality_list])
    pd_result['median_prob'] = pd.Series([np.median(p_vect) for p_vect in p_normality_list])
    pd_result['std_prob'] = pd.Series([np.std(p_vect) for p_vect in p_normality_list])
    # pd_result['accuracy'] = [compute_accuracy(p_norm_vect, 1 if is_normal else 0) for p_norm_vect, is_normal in zip(p_normality_list, pd_result['is_normal'])]


    print(tabulate(pd_result[pd_result['is_normal'] == True].sort_values(by=['mouse_group']), headers='keys', tablefmt="psql", floatfmt=".4f"))
    print(tabulate(pd_result[pd_result['is_normal'] == False].sort_values(by=['mouse_group']), headers='keys', tablefmt="psql", floatfmt=".4f"))

    target_prob = np.array(pd_result['is_normal'])
    print("auc with mean", compute_auc(target_prob, pd_result['mean_prob'].to_numpy(), pos_label=1))
    print("auc with median", compute_auc(target_prob, pd_result['median_prob'].to_numpy(), pos_label=1))