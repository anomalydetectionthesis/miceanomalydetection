import itertools
import math
import time


def _parse_total(iterable, total):
    if total is None:
        if hasattr(iterable, '__len__'):
            total = len(iterable)
    else:
        assert total > 0
        if hasattr(iterable, '__len__'):
            assert total <= len(iterable)

    return total


class Progress():
    def __init__(self, iterable, total: int=None, force_stop=False, bar_size=30, min_refresh_freq=0.5, show=True):
        self.iterable = iterable
        self.total = _parse_total(iterable, total)
        self.show = show
        self.bar_size = bar_size
        self.min_refresh_freq = min_refresh_freq
        self.force_stop = force_stop

        self.mode = 'progress' if self.total is not None else 'count'

    def __iter__(self):

        count = 0

        t0 = time.time()
        update_time = time.time() - self.min_refresh_freq
        iterable = self.iterable

        # Force iteration stop when self.total elements has been iterated
        if self.force_stop and self.total is not None:
            iterable = itertools.islice(self.iterable, self.total)

        for elem in iterable:
            if self.show:
                if time.time() - update_time >= self.min_refresh_freq:
                    update_time = time.time()
                    if self.mode == 'progress':
                        self.show_bar(count, t0)
                    else:
                        self.show_count(count, t0)
            yield elem

            count += 1

        if self.show:
            if self.mode == 'progress':
                self.show_bar(self.total, t0, end=True)
            else:
                print()

    def show_count(self, count, start_time):
        elapsed_time = f"{'%.3f' % (time.time() - start_time)}"

        if self.total is None:
            progress_count = f'{count}'
        else:
            progress_count = f'{count}/{self.total}'

        print('\r', end='')
        print(f'[{progress_count}] {elapsed_time}s', end='')

    def show_bar(self, count, start_time, end=False):
        n_step = math.floor((count / self.total) * self.bar_size)
        n_step = n_step if n_step < self.bar_size else self.bar_size
        n_spaces = self.bar_size - n_step

        steps = '=' * n_step
        spaces = '.' * n_spaces
        bar_str = f'|{steps}{spaces}|'

        remaining_steps = (self.total - count) if (self.total - count) >= 0 else 0
        elapsed_time = f"{'%.3f' % (time.time() - start_time)}"
        remaining_time = '?' if count <= 0 else f"{'%.3f' % ((time.time() - start_time) / count * remaining_steps)}"

        progress = f"[{count}/{self.total}] {elapsed_time}s - {remaining_time}s"

        print('\r', end='')
        print(f'{bar_str}{progress}', end='')

        if end:
            print()


if __name__ == '__main__':
    for i in Progress(iter(range(500)), total=500):
        time.sleep(0.02)
