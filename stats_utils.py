from tabulate import tabulate
import pandas as pd
import matplotlib.pyplot as plt

def show_df(df, headers='keys', table_format = 'psql', number_format='0.4f' ):
    print(
        tabulate(
            df,
            headers=headers,
            tablefmt=table_format,
            floatfmt=number_format
        )
    )

def plot_df(df: pd.DataFrame, kind='bar', **kwargs):
    df.plot(kind=kind, **kwargs)
    plt.show()