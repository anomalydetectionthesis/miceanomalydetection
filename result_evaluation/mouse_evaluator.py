import os

import pandas as pd
import numpy as np

from dataset.base.dataset_model import BaseDataset, BaseVideo
from dataset.dataset_builders.anomaly_score_dataset import AnomalyScoreDataset
from stats_utils import show_df
from result_evaluation.stats_utils import compute_auc
from utils.video_name_info_extractor import video_info


class MouseEvaluator:
    def __init__(self, score_dataset: BaseDataset):
        self.score_dataset = score_dataset

    def compute_stats(self) -> pd.DataFrame:
        df_data={
            'video': [],
            'mouse_class': [],
            'pnd':[],
            'label':[],
            'score_mean': [],
            'score_median': [],
            'score_std': []
        }

        for video in self.score_dataset:
            video: BaseVideo
            video_name = video.video_name
            score_vector = video[:]
            mouse_id, mouse_class, cage, geno, pnd, sex = video_info(video_name).values()

            df_data['video'].append(video_name)
            df_data['mouse_class'].append(mouse_class)
            df_data['label'].append(1 if mouse_class == 'normal' else 0)
            df_data['pnd'].append(pnd)
            df_data['score_mean'].append(np.mean(score_vector))
            df_data['score_median'].append(np.median(score_vector))
            df_data['score_std'].append(np.std(score_vector))


        return pd.DataFrame(df_data)

    def show_summary(self, verbose=False):
        stats = self.compute_stats()
        auc, ci = compute_auc(stats['label'].to_numpy(), stats['score_mean'].to_numpy(), pos_label=1, confidence=0.95)

        pnd_score_df = stats[['pnd', 'mouse_class', 'score_mean']]\
            .groupby(by=['pnd', 'mouse_class'])\
            .mean()\
            .reset_index()\
            .pivot(index='pnd', columns='mouse_class')

        class_score_ds = stats[['mouse_class', 'score_mean']]\
            .groupby(by='mouse_class')\
            .agg(['mean', 'std'])

        show_df(pd.DataFrame({'auc': [auc], 'ci(95%)': [ci]}))
        show_df(class_score_ds)
        show_df(pnd_score_df)

        if verbose:
            show_df(stats[stats['mouse_class'] == 'normal'])
            show_df(stats[stats['mouse_class'] == 'abnormal'])

            # print(tabulate(normal_stats, headers='keys', tablefmt="psql", floatfmt=".4f"))
            # print(tabulate(abnormal_stats, headers='keys', tablefmt="psql", floatfmt=".4f"))



    @staticmethod
    def from_file(score_dataset_path):
        score_dataset = AnomalyScoreDataset.from_hdf(score_dataset_path)
        return MouseEvaluator(score_dataset)


if __name__ == '__main__':
    from paths import ROOT_PATH

    env_path = os.path.join(ROOT_PATH, 'data/mice/frame_prediction/sf10_glr1e-4_dlr1e-5_loss_i1_w1_a1')
    result_folder = os.path.join(env_path, 'prediction_results')

    mouse_evaluator = MouseEvaluator.from_file(result_folder)
    stats: pd.DataFrame = mouse_evaluator.compute_stats()

    normal_stats = stats[stats['mouse_class'] == 'normal']
    abnormal_stats = stats[stats['mouse_class'] == 'abnormal']

    show_df(normal_stats)
    show_df(abnormal_stats)

    auc, ci = compute_auc(stats['label'].to_numpy(), stats['score_mean'].to_numpy(), pos_label=1, confidence=0.95)
    print(auc,ci)