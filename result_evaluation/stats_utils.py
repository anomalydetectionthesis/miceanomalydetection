import numpy as np
from sklearn import metrics
from scipy.stats import norm


def auc_ci(auc, n_pos, n_neg, alpha=0.05):
    # how to compute auc CI:
    # http://www.real-statistics.com/descriptive-statistics/roc-curve-classification-table/auc-confidence-interval/
    q0 = auc * (1 - auc)
    q1 = (auc / (2 - auc)) - (auc ** 2)
    q2 = ((2 * (auc ** 2)) / (1 + auc)) - (auc ** 2)

    se = np.sqrt((q0 + ((n_pos - 1) * q1) + ((n_neg - 1) * q2)) / (n_pos * n_neg))
    ci = se * norm.ppf(1 - (alpha / 2))
    return ci


def compute_auc(label_vector: np.ndarray, score_vector: np.ndarray, pos_label=0, confidence=0.95):
    '''
    Args:
    - pos_label: the positive label value
    - confidence: confidence level used to compute auc CI
    '''

    fpr, tpr, thresholds = metrics.roc_curve(label_vector, score_vector, pos_label=pos_label)
    auc = metrics.auc(fpr, tpr)

    n_pos = np.sum(label_vector == pos_label)
    n_neg = np.sum(label_vector != pos_label)

    ci = auc_ci(auc, n_pos, n_neg, 1 - confidence)
    return auc, ci
