import numpy as np
import torch

def sum_square_errors(input, target):
    return torch.sum((input - target) ** 2)


def compute_psnr(pred_img: torch.Tensor, gt_img: torch.Tensor, max_pixel_value = 1):
    '''
    Pixel values must be in [0, 1] range
    '''
    assert pred_img.shape == gt_img.shape
    img_shape = pred_img.shape

    num_pixels = np.prod(img_shape)
    sse = sum_square_errors(pred_img, gt_img)
    numerator = max_pixel_value**2
    denominator = sse/num_pixels

    return 10*torch.log10(numerator/denominator)


def anomaly_score(psnr_vector):
    '''
    Args:
        - psnr_vector (np.array): The psnr vector for some video
    '''
    assert len(psnr_vector.shape)==1

    psnr_max = np.max(psnr_vector)
    psnr_min = np.min(psnr_vector)
    return (psnr_vector - psnr_min)/(psnr_max - psnr_min)

if __name__ == '__main__':
    img1 = torch.tensor([
        [
            [[1., 1.], [1, 1]],
            [[1, 1], [1, 1]],
            [[1, 1], [1, 1]]
        ],
        [
            [[1, 1], [1, 1]],
            [[1, 1.], [1, 1]],
            [[1, 1], [1, 1]]
        ]
    ])

    img2 = torch.tensor([
        [
            [[0, 0], [0, 0]],
            [[0, 0.], [0, 0]],
            [[0, 0.], [0, 0]]
        ],
        [
            [[0, 0], [0, 0]],
            [[0, 0], [0, 0]],
            [[0, 0], [0, 0]]
        ]
    ])

    print(compute_psnr(img1, img1))
    print(compute_psnr(img1, img2))