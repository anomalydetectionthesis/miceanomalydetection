import itertools
from typing import Iterable
import cv2

from dataset.dataset_builders.frame_dataset import FrameDataset
from dataset.dataset_builders.predictions_dataset import PredictionsDataset
from dataset.frame.thermal_frame import ThermalFrame

from progress_bar import Progress
from result_evaluation.result_video.prediction_result_frame_creator import PredictionResultFrameCreator



def export_video():
class VideoCreator:

    def swap_br_channels(self, frame):
        return frame[..., ::-1]  # RGB <-> BGR

    def create_video(self, frame_iterator: Iterable, frame_limit, dest_video_path, fps=30):

        video = None

        for frame in Progress(frame_iterator, frame_limit, force_stop=True):

            height, width, layers = frame.shape

            if video is None:
                video = cv2.VideoWriter(dest_video_path, cv2.VideoWriter_fourcc(*'DIVX'), fps, (width, height))

            video.write(self.swap_br_channels(frame))

        video.release()


# def create_result_videos( results_ds: BaseDataset, dest_folder_path, n_frames = None):
#     assert os.path.exists(dest_folder_path) and os.path.isdir(dest_folder_path)
#
#     result_eval = ResultVideoCreator()
#
#     count = 0
#     n_videos = results_ds.n_videos()
#
#     for video_name in results_ds.keys():
#         count += 1
#         video_result_ds = results_ds.get_video(video_name)
#         print(f'[{count}/{n_videos}]: Dataset -> {video_result_ds.get_dataset_name()} \t Video -> {video_result_ds.get_video_name()}')
#
#         video_path = os.path.join(dest_folder_path, f'{video_name}.avi')
#
#         result_eval.create_video(video_result_ds, video_path, frame_limit=n_frames)


def test_prediction_results_video_creation(prediction_results_path):
    results_ds = PredictionsDataset.from_hdf(prediction_results_path, ThermalFrame.from_numpy)
    video_ds = results_ds[0]

    frame_limit = 200
    frame_creator = PredictionResultFrameCreator(frame_limit)
    frame_iter = (frame_creator.get_result_frame(*results) for results in video_ds)

    vid_creator = VideoCreator()
    vid_creator.create_video(frame_iter, frame_limit=frame_limit, dest_video_path='/home/edo/Desktop/mouse0.avi')

def test_frame_dataset_video_creation(ds_path):
    dataset = FrameDataset.from_hdf(ds_path, ThermalFrame.from_numpy)
    video = dataset[0]
    vid_name = video.video_name
    frame_iter = (frame.to_rgb_image() for frame in video)
    vid_creator = VideoCreator()
    vid_creator.create_video(frame_iter, frame_limit=400, dest_video_path=f'/home/edo/Desktop/{vid_name}.avi')

if __name__ == '__main__':
    # predictions_ds_path = '/home/edo/Documents/Tesi/MiceAnomalyDetection/data/mice/frame_prediction/sf10_glr1e-4_dlr1e-5_loss_i1_w1_a1/prediction_results/wt_2020_02_18_pnd8.h5'
    # test_prediction_results_video_creation(predictions_ds_path)

    frame_ds_path = '/media/edo/edo/ds/cdkl5_6b1_2020-02-21_01.h5'
    test_frame_dataset_video_creation(frame_ds_path)
