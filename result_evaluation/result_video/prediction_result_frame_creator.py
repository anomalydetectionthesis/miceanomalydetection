import matplotlib.pyplot as plt
import numpy as np
from PIL import Image

from dataset.frame.frame import Frame


class PredictionResultFrameCreator:
    def __init__(self, tot_frames):
        self.tot_frames = tot_frames

    def to_gray_scale(self, frame):
        if frame.ndim == 2:
            return frame

        return np.dot(frame[..., :3], [0.2989, 0.5870, 0.1140])

    def get_frame_diff(self, gt_frame: Frame, pred_frame: Frame, fig_size=None):
        gray_gt_frame = self.to_gray_scale(gt_frame.to_numpy())
        gray_pred_frame = self.to_gray_scale(pred_frame.to_numpy())
        frame_diff = np.abs(gray_gt_frame - gray_pred_frame)

        fig = plt.figure(figsize=fig_size)
        fig.add_subplot(111)
        plt.imshow(frame_diff, cmap='gray')
        plt.title('Diff')

        fig.tight_layout(pad=0)
        fig.canvas.draw()

        # Save fig in a numpy array.
        rgb_string = fig.canvas.tostring_rgb()
        canvas_shape = fig.canvas.get_width_height()
        pil_image = Image.frombytes("RGB", canvas_shape, rgb_string)
        frame = np.array(pil_image)
        plt.close(fig)

        return frame

    def get_score_plot(self, score_vector, fig_size=None):
        fig = plt.figure(figsize = fig_size)
        fig_ax = fig.add_subplot(111)

        frame_num = len(score_vector)
        time_axis = range(frame_num)
        plt.plot(time_axis, score_vector)
        plt.ylim(-0.1, 1.1)
        plt.xlim(0, self.tot_frames)
        fig_ax.set_ylabel('Score - (Higher is Better)')
        fig_ax.set_xlabel('Frame n.')

        fig.tight_layout(pad=0)
        fig.canvas.draw()

        # Save fig in a numpy array.
        rgb_string = fig.canvas.tostring_rgb()
        canvas_shape = fig.canvas.get_width_height()
        pil_image = Image.frombytes("RGB", canvas_shape, rgb_string)
        score_plot = np.array(pil_image)
        plt.close(fig)

        return score_plot


    def get_result_frame(self, gt_frame: Frame, pred_frame: Frame, score_vector):
        assert gt_frame.shape == pred_frame.shape

        fig = plt.figure(figsize=(9.5, 6.5))

        fig.add_subplot(2, 3, 1)
        plt.imshow(gt_frame.to_rgb_image(title='Target_img', fig_size=(3,3)))
        plt.axis('off')

        fig.add_subplot(2, 3, 2)
        plt.imshow(pred_frame.to_rgb_image(title='Gen_img', fig_size=(3,3)))
        plt.axis('off')

        fig.add_subplot(2, 3, 3)
        plt.imshow(self.get_frame_diff(gt_frame, pred_frame, fig_size=(3,3)))
        plt.axis('off')

        fig.add_subplot(2, 1, 2)
        plt.imshow(self.get_score_plot(score_vector, fig_size=(9,2.5)))
        plt.axis('off')

        plt.tight_layout(pad=2.5, w_pad=1.5, h_pad=2.5)
        fig.canvas.draw()


        rgb_string = fig.canvas.tostring_rgb()
        canvas_shape = fig.canvas.get_width_height()
        pil_image = Image.frombytes("RGB", canvas_shape, rgb_string)
        data = np.array(pil_image)
        plt.close(fig)

        return data