import os

import numpy as np

from dataset.base.dataset_model import BaseDataset, BaseVideo
from dataset.dataset_builders.anomaly_score_dataset import AnomalyScoreDataset
from paths import ROOT_PATH

from result_evaluation.frame_level.anomalous_frame_parser import AnomalousFramesParser
from result_evaluation.stats_utils import compute_auc


class FrameLevelEvaluator:
    def __init__(self, score_dataset: BaseDataset, annotations_dataset: AnomalousFramesParser):
        self.score_dataset = score_dataset
        self.annotations_dataset = annotations_dataset

    def compute_AUC(self, confidence = 0.95):

        score_vector = np.array([])
        label_vector = np.array([])
        for video in self.score_dataset:
            video: BaseVideo
            video_score_vect = np.array(video[:])

            labels = self.annotations_dataset.get_class_vector(video.video_name)

            score_vector = np.concatenate((score_vector, video_score_vect))
            label_vector = np.concatenate((label_vector, labels))

        normal_frames_mean = np.mean(score_vector[label_vector == 0])
        abnormal_frames_mean = np.mean(score_vector[label_vector == 1])

        auc, ci = compute_auc(label_vector, score_vector, pos_label=0, confidence=confidence)

        return auc, ci, normal_frames_mean, abnormal_frames_mean

    @staticmethod
    def from_files(score_dataset_path:str, annotations_dataset_path: str, n_stacked_frames: int):
        score_dataset = AnomalyScoreDataset.from_hdf(score_dataset_path)
        annotations_dataset = AnomalousFramesParser(annotations_dataset_path, n_stacked_frames)

        return FrameLevelEvaluator(score_dataset, annotations_dataset)


if __name__ == '__main__':
    score_dataset_path = os.path.join(ROOT_PATH, 'data/ped2/adv_ped2_results.hdf5')
    annotations_file_path = os.path.join(ROOT_PATH, 'data/ped2/annotations.json')
    n_stacked_frames = 10

    result_evaluation = FrameLevelEvaluator.from_files(score_dataset_path, annotations_file_path, n_stacked_frames)

    auc, ci, normal_frames_mean, abnormal_frames_mean = result_evaluation.compute_AUC(confidence = 0.95)
    print(f'AUC: {auc} (\u00B1{ci})\t [{auc-ci} - {auc+ci}]')
    print(f'normal_mean: {normal_frames_mean}\tabnormal_mean: {abnormal_frames_mean}')