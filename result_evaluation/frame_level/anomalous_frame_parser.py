import json
import numpy as np

# Il file delle annotazioni deve avere un oggetto per ogni video
# "nome_video":{
#   n_frames: 10
#   anomalous_frames: [
#       {"from": 0, "to": 10},
#       ...
#   ]
# }
# n_frames numero => di frame in quel video
# anomalous_frames => lista di intervalli di frame anomali
# Gli indici dei frame devono essere:
# - 0 => primo frame del video
# - n_frames-1 => ultimo frame del video
#
# L'intervallo di frame anomali ha 2 indici:
# - from: l'indice del primo frame anomalo (incluso)
# - to: l'indice del ultimo frame(escluso)
# es.
#      {"from": 0, "to": 10} => frame anomali da 0 a 9. Il 10 non è anomalo

class AnomalousFramesParser:
    def __init__(self, file_path, n_stacked_frames):
        self.file_path = file_path
        self.n_stacked_frames = n_stacked_frames

    def parse_video_metadata(self, video_metadata):
        assert 'n_frames' in video_metadata
        assert 'anomalous_frames' in video_metadata

        n_frames = video_metadata['n_frames']
        anomalous_frames = video_metadata['anomalous_frames']

        return n_frames, anomalous_frames

    def _get_indexes(self, frames_group):
        assert 'from' in frames_group
        assert 'to' in frames_group

        start = frames_group['from']
        end = frames_group['to']

        return start, end


    def get_class_vector(self, video):
        '''
            Return:
                 np.array(n_frames): [0,0,0,0,....1,1,1,1,0,0,0]
                 0 -> normal frame
                 1 -> abnormal frame
        '''
        with open(self.file_path, 'r') as anomalous_frames_file:
            metadata_file = json.load(anomalous_frames_file)

            assert video in metadata_file
            video_metadata = metadata_file[video]
            n_frames, anomalous_frames = self.parse_video_metadata(video_metadata)

            class_vector = np.zeros(n_frames)

            for frames_group in anomalous_frames:
                start, end = self._get_indexes(frames_group)
                class_vector[start:end] = 1

            return class_vector[self.n_stacked_frames:]