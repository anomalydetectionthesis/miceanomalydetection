from sklearn.utils._random import sample_without_replacement

from clustering.bow_score_model import BowScoreModel
from clustering.dist_score_model import DistScoreModel
from clustering.kmeans.k_means_model import KMeansModel
from clustering.mice_clustering_evaluation import ClusteringEvaluation
from clustering.mouse_clustering_dataset import MouseClusteringDataset
from dataset.prediction_split import PredictionSplit
import numpy as np

if __name__ == '__main__':
    import os
    from paths import ROOT_PATH

    k = 5
    sf = 10
    # sim_th = 0.96
    pool_type = 'avg'

    env_path = os.path.join(ROOT_PATH, f'data/mice/frame_prediction/sf{sf}')
    latent_folder = os.path.join(env_path, 'latent_results')
    # model_file_name = f'k{k}_sf{sf}_{pool_type}_sim{int(sim_th*100)}_model'
    model_file_name = f'k{k}_sf{sf}_{pool_type}_model'
    kmeans_models_folder = os.path.join(env_path, f'kmeans_models')
    model_file_path = os.path.join(kmeans_models_folder, f'{model_file_name}.pkl')
    csv_file_path = os.path.join(env_path, f'{model_file_name}.csv')

    clustering_ds = MouseClusteringDataset.from_folder(latent_folder, pool_type, video_list=PredictionSplit.full)
    data_dict = clustering_ds.to_dict(filter_fn=lambda vid_name: 'wt' not in vid_name)

    print("Extract K-means train data")
    sample_indexes = sample_without_replacement(n_population=len(data_dict), n_samples=50000, random_state=50495839)
    latent_data = np.array(list(data_dict.values()))[sample_indexes]
    print(len(latent_data))
    print("Fit Kmeans")
    model = KMeansModel.fit(k, latent_data, shuffle=False)

    print("Bow Evaluation")
    bow_score_model = BowScoreModel(model, clustering_ds)
    score_df = bow_score_model.get_score_df(clustering_ds)

    ClusteringEvaluation.evaluate(score_df)

    print("Sim Evaluation")
    dist_score_model = DistScoreModel(model, train_label=1)
    score_df = dist_score_model.get_score_df(clustering_ds)

    ClusteringEvaluation.evaluate(score_df)
