import pickle
import sklearn
import numpy as np
from sklearn.cluster import KMeans, MiniBatchKMeans
from sklearn.metrics import silhouette_score
from clustering.clustering_model import ClusteringModel


class KMeansModel(ClusteringModel):

    def __init__(self, model: KMeans):
        self.model = model

    @property
    def n_clusters(self):
        return self.model.n_clusters

    @property
    def cluster_labels(self):
        return list(range(self.n_clusters))

    def predict(self, X: np.ndarray, progress=False, **kwargs):
        assert X.ndim in [1, 2]

        if X.ndim == 1:
            X = np.array([X])

        labels = self.model.predict(X)
        centroids = np.array(self.model.cluster_centers_)[labels]
        assert len(labels) == len(centroids)
        diffs = X-centroids
        # np.linalg.norm(X-centroids,ord=2, axis=1)**2
        sqr_dist = np.einsum('ij, ij->i', diffs, diffs)
        return labels, sqr_dist

    # def score(self, test_data, random_state=3094951):
    #     # the silhouette_score score automatically select a sample_size random samples
    #     test_labels = self.model.predict(test_data)
    #     score = silhouette_score(test_data, labels=test_labels, random_state=random_state)
    #     return score

    @staticmethod
    def fit(k, train_data: np.ndarray, shuffle=False, minibatch=None, random_state=3094951) -> 'KMeansModel':

        if shuffle:
            train_data = sklearn.utils.shuffle(train_data, random_state=random_state)

        if minibatch is not None:
            model = MiniBatchKMeans(n_clusters=k, batch_size=minibatch, n_init=5, max_no_improvement=10, random_state=random_state, verbose=0,)
        else:
            model = KMeans(n_clusters=k, n_init=5, tol=1e-8, random_state=random_state, verbose=0)

        model.fit(train_data)
        return KMeansModel(model)

    @staticmethod
    def load(model_file_path) -> 'KMeansModel':
        with open(model_file_path, 'rb') as model_file:
            model = pickle.load(model_file)
            return KMeansModel(model)

    def save(self, model_file_path):
        with open(model_file_path, 'wb') as model_file:
            pickle.dump(self.model, model_file)