#
# from clustering.k_means_model import KMeansModel
#
# from progress_bar import Progress
#
#
# def get_model_file_path(kmeans_models_folder, k):
#     assert os.path.exists(kmeans_models_folder)
#
#     model_file_name = f'k{k}_model.pkl'
#     return os.path.join(kmeans_models_folder, model_file_name)
#
# def load_model(k, kmeans_models_folder):
#     model_file_path = get_model_file_path(kmeans_models_folder, k)
#
#     if os.path.exists(model_file_path):
#         return KMeansModel.load_model(model_file_path)
#     else:
#         raise FileNotFoundError
#
#
# # def hierarchical_eval(latent_data: MouseLatentData):
# #
# #     data = latent_data.get_latent_vecs(normalize=True, n_samples=300)
# #     model = HierarchicalModel.fit_model(data)
# #     model.show(n_splits=5)
#
#
# def kmeans_eval(latent_vecs, max_k=10, kmeans_models_folder=None):
#     assert os.path.exists(kmeans_models_folder)
#
#     score_vect=[]
#
#     for k in Progress(range(2,max_k+1), total= max_k-1):
#         try:
#             model = load_model(k, kmeans_models_folder)
#         except FileNotFoundError:
#             model = KMeansModel.fit_model(k, latent_vecs)
#             model.save_model(get_model_file_path(kmeans_models_folder, k))
#
#         score_vect.append(model.score(latent_vecs))
#
#     return score_vect

# def score(self, test_data, random_state=3094951):
#     # the silhouette_score score automatically select a sample_size random samples
#     test_labels = self.model.predict(test_data)
#     score = silhouette_score(test_data, labels=test_labels, random_state=random_state)
#     return score
#
#
# if __name__ == '__main__':
#     import os
#     from paths import ROOT_PATH
#
#     pool_type = 'max'
#     env_path = os.path.join(ROOT_PATH, 'data/mice/frame_prediction/sf5')
#
#     latent_folder = os.path.join(env_path, 'latent_results')
#     latent_data = MouseTrainDataset.from_folder(latent_folder, pool_type=pool_type).filter(sim_th=0.91)
#     kmeans_models_folder = os.path.join(env_path, f'{pool_type}_kmeans_models')
#
#     score_vect = kmeans_eval(latent_data, kmeans_models_folder=kmeans_models_folder, max_k=20)
#     print(score_vect)
#
#     #hierarchical_eval(latent_data)
#
#
#
