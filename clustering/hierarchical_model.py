# import sklearn
# import scipy.cluster.hierarchy as shc
# import matplotlib.pyplot as plt
# import numpy as np
#
# from clustering.mouse_data_plot import MouseTrainDataset
#
#
# class HierarchicalModel:
#
#     def __init__(self, clusters):
#         self.clusters = clusters
#
#     def show(self, n_splits = None):
#         plt.figure(figsize=(10, 7))
#         plt.title("Dendrograms")
#         plt.xlabel('')
#         plt.ylabel('ward distance')
#
#         print("show clusters")
#         dend = shc.dendrogram(
#             self.clusters,
#             truncate_mode='lastp',  # show only the last p merged clusters
#             p=10,  # show only the last p merged clusters
#             show_leaf_counts=True,  # otherwise numbers in brackets are counts
#             leaf_rotation=90.,
#             show_contracted=True,
#         )
#
#         # plt.axhline(y=6, color='r', linestyle='--')
#         plt.show()
#
#     @staticmethod
#     def fit_model(train_data: np.ndarray, shuffle=False, random_state=3094951) -> 'HierarchicalModel':
#
#         if shuffle:
#             train_data = sklearn.utils.shuffle(train_data, random_state=random_state)
#         clusters = shc.linkage(train_data, method='ward')
#
#         return HierarchicalModel(clusters)
#
#
#     @staticmethod
#     def _normalize(data: np.ndarray, mean, std):
#         return (data-mean)/std
#
#     # @staticmethod
#     # def load_model(model_file_path) -> 'HierarchicalModel':
#     #     with open(model_file_path, 'rb') as model_file:
#     #         model = pickle.load(model_file)
#     #         return HierarchicalModel(model)
#     #
#     # def save_model(self, model_file_path):
#     #     with open(model_file_path, 'wb') as model_file:
#     #         pickle.dump({}, model_file)
#
#
# if __name__ == '__main__':
#     import os
#     from paths import ROOT_PATH
#
#     env_path = os.path.join(ROOT_PATH, 'data/mice/frame_prediction/sf10')
#     latent_folder = os.path.join(env_path, 'latent_results')
#     train_ds = MouseTrainDataset.from_folder(latent_folder,'max').filter(sim_th=0.94)
#     model = HierarchicalModel.fit_model(train_ds.latent_vecs)
#     model.show(n_splits=20)