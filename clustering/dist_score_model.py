from clustering.clustering_model import ClusteringModel
import numpy as np
import pandas as pd


class DistScoreModel:
    def __init__(self, model: ClusteringModel, train_label=None):
        # train_label 0 = model trained with normal features
        # train_label 1 = model trained with abnormal features
        assert train_label in [0, 1]
        self.model = model
        self.train_label = train_label

    def compute_score(self, latent_data):
        labels, sqr_dist = self.model.predict(latent_data)
        score = np.mean(sqr_dist)
        if self.train_label == 1:
            score = 1 -score
        return score

    def get_score_df(self, dataset) -> pd.DataFrame:
        df_data = []
        for video_name, latent_data in dataset:
            df_data.append({
                'video': video_name,
                'label': self._get_label(video_name),
                'score': self.compute_score(latent_data)
            })
        return pd.DataFrame(df_data)

    def _get_label(self,video_name):
        return 0 if 'wt' in video_name else 1