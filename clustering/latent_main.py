from clustering.mouse_clustering_dataset import MouseClusteringDataset

if __name__ == '__main__':
    import os
    from paths import ROOT_PATH

    k = 4
    sf = 10
    sim_th = 0.96
    pool_type = 'avg'

    env_path = os.path.join(ROOT_PATH, f'data/mice/frame_prediction/sf{sf}')
    latent_folder = os.path.join(env_path, 'latent_results')
    model_file_name = f'k{k}_sf{sf}_{pool_type}_sim{int(sim_th*100)}_model'
    kmeans_models_folder = os.path.join(env_path, f'kmeans_models')
    model_file_path = os.path.join(kmeans_models_folder, f'{model_file_name}.pkl')
    csv_file_path = os.path.join(env_path, f'{model_file_name}.csv')


    clustering_ds = MouseClusteringDataset.from_folder(latent_folder, pool_type)
    if os.path.exists(model_file_path):
        print('Load kmeans')
        model = KMeansModel.load_model(model_file_path)
    else:
        print('Fit kmeans')
        # todo reimplement similarity filtering
        latent_vecs, _, _ = clustering_ds.to_array()
        print('start model fit')
        model = KMeansModel.fit_model(k, latent_vecs)
        model.save_model(model_file_path)


    #bow_results = MouseBowResults.compute_from_kmeans(model, clustering_ds)
    #bow_results.to_csv(csv_file_path)
    #bow_results.compute_auc(lambda bow: bow[2] + bow[0] - bow[1] - bow[3])
    #bow_results.to_csv('/home/edo/Desktop/k3_model.csv')

    #score_fn = lambda bow: (bow[1]/np.sum(bow)) - (bow[0]/np.sum(bow)) - (bow[3]/np.sum(bow))

    #bow_results.show_score_summary()

    #bow_results.show_results_summary()
    # bow_results.show_pca()
    # bow_results.show_tsne()