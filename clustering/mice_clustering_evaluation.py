from clustering.mouse_clustering_dataset import MouseClusteringDataset
from progress_bar import Progress
import numpy as np
import pandas as pd

from result_evaluation.stats_utils import compute_auc
from stats_utils import show_df, plot_df
from utils.video_name_info_extractor import video_info


def _compute_auc_from_df(eval_df):
    label_vector = eval_df['label']

    if len(np.unique(label_vector)) == 2:
        score_vector = eval_df['score'].values
        auc, ci = compute_auc(label_vector, score_vector, pos_label=1, confidence=0.95)
        return pd.Series([auc, ci], index=['auc', 'ci'])
    else:
        return None


def _preprocess_df(score_df: pd.DataFrame):
    eval_df = score_df[['video', 'score', 'label']].copy()

    eval_df['class'] = eval_df['video'].apply(lambda vid_name: 'normal' if 'wt' in vid_name else 'abnormal')
    eval_df['pnd'] = eval_df['video'].apply(lambda vid_name: video_info(vid_name)['pnd'])
    eval_df['cage'] = eval_df['video'].apply(lambda vid_name: video_info(vid_name)['cage'])

    return eval_df


def _validate_score_dataframe(score_df: pd.DataFrame):
    assert all(col in ['video', 'score', 'label'] for col in score_df.columns)


class ClusteringEvaluation:

    @staticmethod
    def evaluate(score_df: pd.DataFrame):
        _validate_score_dataframe(score_df)
        eval_df = _preprocess_df(score_df)

        # Full DF auc
        auc, ci = _compute_auc_from_df(eval_df)
        show_df({'auc': [auc], 'ci': [ci]})

        # Mean score per PND
        pnd_score: pd.DataFrame = eval_df\
            .groupby(by=['pnd', 'class'],as_index=False)['score']\
            .apply(lambda score: pd.Series([score.mean(), score.median()],index=['avg_score', 'median_score']))\
            .reset_index()

        #plot_df(pnd_score.pivot(index='pnd', columns='is_normal', values='avg_score'), kind='bar')
        show_df(pnd_score.pivot(index='pnd', columns='class'))

        # auc per PND
        pnd_auc = eval_df[['pnd', 'class', 'score', 'label']].groupby('pnd').apply(
            lambda df: _compute_auc_from_df(df))

        show_df(pnd_auc)

    @staticmethod
    def from_sim_score(model, mouse_test_data: MouseClusteringDataset):
        df_data = []
        for video_name, latent_vecs in Progress(mouse_test_data):
            label_list, sim_list = model.predict(latent_vecs)

            df_data.append({
                'video': video_name,
                'score': np.mean(sim_list)
            })


        ClusteringEvaluation.evaluate(pd.DataFrame(df_data))