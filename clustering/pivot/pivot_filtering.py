import pandas as pd
from clustering.utils.clusters_stats import ClustersStats
from utils.video_name_info_extractor import video_info


def pivot_filtering_dto(clusters_stats:ClustersStats):
    def filter_pivot_stats(pivot_key, pivot_stats):
        pivot_id, pivot_class, pivot_cage, pivot_geno, pivot_pnd, pivot_sex = video_info(pivot_key).values()
        return {
            'pivot': pivot_key,
            'pivot_class': pivot_class,
            'pivot_pnd': pivot_pnd,
            'total': pivot_stats['cluster_stats']['total'],
            'class': pivot_stats['cluster_stats'][pivot_class],
            'class_cage': pivot_stats['cage_stats'][pivot_class].get(pivot_cage, 0),
            'class_mouse': pivot_stats['mice_stats'][pivot_class].get(pivot_id, 0),
            'class_pnd': pivot_stats['pnd_stats'][pivot_class].get(pivot_pnd, 0),
            'class_sex': pivot_stats['sex_stats'][pivot_class].get(pivot_sex, 0),
        }

    stats_list = clusters_stats.map_stats(filter_pivot_stats)
    stats_df= pd.DataFrame(stats_list)
    return stats_df


class PivotFiltering:
    def __init__(self, size_th=500, classification_th=0.9, mouse_overfit_th=0.4, cage_overfit_th=0.6):
        self.size_th = size_th
        self.classification_th = classification_th
        self.mouse_overfit_th = mouse_overfit_th
        self.cage_overfit_th = cage_overfit_th

    def __call__(self, clusters_stats):
        return self.filter_pivot(clusters_stats)

    # size_th=500, classification_th=0.9, mouse_overfit_th=0.4, cage_overfit_th=0.6
    def filter_pivot(self, clusters_stats):
        stats_df = pivot_filtering_dto(clusters_stats)

        stats_df = stats_df[stats_df['class'] > self.size_th]

        stats_df['classification_score'] = stats_df['class'] / stats_df['total']
        stats_df['mouse_overfit_score'] = (stats_df['class_mouse'] / stats_df['class'])

        stats_df['cage_overfit_score'] = stats_df['class_cage'] / stats_df['class']
        stats_df = stats_df[stats_df['classification_score'] >= self.classification_th]

        stats_df = stats_df[stats_df['mouse_overfit_score'] <= self.mouse_overfit_th]
        stats_df = stats_df[stats_df['cage_overfit_score'] <= self.cage_overfit_th]

        cols = [
            'pivot',
            'pivot_class',
            'pivot_pnd',
            'total',
            'class',
            'class_cage',
            'class_pnd',
            'class_mouse',
            'classification_score',
            'mouse_overfit_score',
            'cage_overfit_score'
        ]

        print_df = stats_df[cols].sort_values(
            by=['pivot_class', 'total', 'class', 'classification_score', 'mouse_overfit_score', 'cage_overfit_score'],
            ascending=[False, False, True, True, False, False])
        return print_df


        # lt3_mice = stats_df[stats_df['pivot_pnd'] < 3]
        # # print(lt3_mice['pivot_pnd'].value_counts())
        # # lt3_mice = pivot_filtering.filter_pivot(lt3_mice, size_th=500, classification_th=0, mouse_overfit_th=0.35, cage_overfit_th=0.4)
        # lt3_mice = pivot_filtering.filter_pivot(lt3_mice, size_th=500, classification_th=0.8, mouse_overfit_th=0.4,
        #                                         cage_overfit_th=0.6)
        # show_df(lt3_mice)
        # ge3_mice = stats_df[stats_df['pivot_pnd'] >= 3]
        # # print(ge3_mice['pivot_pnd'].value_counts())
        # ge3_mice = pivot_filtering.filter_pivot(ge3_mice, size_th=500, classification_th=0.8, mouse_overfit_th=0.35,
        #                                         cage_overfit_th=0.4)
        # show_df(ge3_mice)
        # filtered_df = lt3_mice.append(ge3_mice, ignore_index=False)
        # filtered_pivots = np.concatenate((lt3_mice['pivot'].to_list(), ge3_mice['pivot'].to_list()))

