import itertools
import os

import cv2

from dataset.base.dataset_model import BaseDataset
from dataset.dataset_builders.video_fragment_dataset import VideoFragmentDataset
from dataset.frame.thermal_frame import ThermalFrame
import pandas as pd
from paths import ROOT_PATH


def show_video(frames, title='video', start=0, end=None, fps=30):
    for frame in itertools.islice(frames, start, end):
        frame = frame.to_rgb_image()[..., ::-1]  # RGB <-> BGR
        cv2.imshow(title, frame)
        wait_ms = 1000 // fps
        cv2.waitKey(wait_ms)

    cv2.destroyAllWindows()


def show_video_fragment(dataset: BaseDataset, pivot_key):
    vid_info, mouse_info, vid_index = pivot_key.split('__')

    vid_name = f'{vid_info}__{mouse_info}'
    vid_index = int(vid_index)

    frame_list = dataset[vid_name][vid_index]
    show_video(frame_list, vid_name)


if __name__ == '__main__':
    sf=10
    pool_type='avg'
    latent_data_path = os.path.join(ROOT_PATH, f'data/mice/frame_prediction/sf{sf}/latent_results')
    clustering_data_path = os.path.join(ROOT_PATH, 'data/mice/clustering')
    ds_path = '/media/edo/edo/ds'

    ds: BaseDataset = VideoFragmentDataset.from_hdf(ds_path, n_stacked_frames=sf, numpy_to_frame_fn=ThermalFrame.from_numpy)

    # pivot_df: pd.DataFrame = pd.read_csv(os.path.join(clustering_data_path, f'sf{sf}_{pool_type}.csv'))
    # pivot_keys = [f'{video}__{index}' for video, index in pivot_df[['video_name', 'video_index']].values]
    pivot_df: pd.DataFrame = pd.read_csv(os.path.join(clustering_data_path, f'sf{sf}_{pool_type}_filtered.csv'))
    pivot_keys = pivot_df['pivot_key'].to_list()

    show_video_fragment(ds, pivot_keys[15])