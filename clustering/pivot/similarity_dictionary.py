import numpy as np


class SimilarityDictionary:
    """
    The similarity dictionary is used in the pivot selection algorithm.
    It is used in order to reduce memory usage during the selection algorithm.
    To reduce memory usage the data structure keep deep feature indexes insead of deep features itself.
    The similarity dictionary contains two dictionaries:
    - sim_keys_dict: keep the list of the deep features indexes that have a specific sim value from a pivot
    - key_sim_dict: given a deep feature index return the similarity corresponding to the nearest pivot
    """
    def __init__(self, data_keys):

        self.sim_keys_dict = {}  # sim-> set(latent_index1, latent_index2, ...)
        self.key_sim_dict = {key: 0 for key in data_keys}  # latent_index-> sim

    def get_latent_indexes(self, sim):
        return list(self.sim_keys_dict.get(sim, set()))

    def get_sim(self, key):
        return self.key_sim_dict.get(key, 0)

    def change_sim(self, key, new_sim):
        self.remove_latent_index(key)
        self.add_latent_index(key, new_sim)

    def add_latent_index(self, key, sim=0):
        self.key_sim_dict[key] = sim
        if sim not in self.sim_keys_dict:
            self.sim_keys_dict[sim] = set()

        curr_set = self.sim_keys_dict[sim]
        self.sim_keys_dict[sim] = curr_set | set(tuple([key]))

    def remove_latent_index(self, key):
        sim = self.key_sim_dict[key]

        if sim in self.sim_keys_dict:
            curr_set = self.sim_keys_dict[sim]
            new_set = curr_set - set(tuple([key]))
            if len(new_set) == 0:
                self.sim_keys_dict.pop(sim)
            else:
                self.sim_keys_dict[sim] = new_set

        self.key_sim_dict.pop(key)

    def remove_sim(self, sim):
        if sim in self.sim_keys_dict:

            latent_indexes = self.get_latent_indexes(sim)

            for index in latent_indexes:
                index_sim = self.key_sim_dict.pop(index)
                assert index_sim == sim

            self.sim_keys_dict.pop(sim)

    def filter_similarites(self, sim_th):
        assert sim_th > 0 and sim_th < 1

        similarities = np.array(self.get_sorted_similarities())
        similarities = similarities[similarities >= sim_th].tolist()

        for sim in similarities:
            self.remove_sim(sim)

    def get_sorted_similarities(self):
        sorted_sim = sorted(self.sim_keys_dict.keys(), reverse=False)
        return sorted_sim

    def __iter__(self):
        return iter(self.key_sim_dict.items())
