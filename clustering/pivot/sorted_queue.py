import numpy as np


class SortedQueue:
    def __init__(self, ascending=True, max_size=0):
        self.max_size = max_size
        self.ascending = ascending
        self.dist_list = []
        self.data_index_list = []

    def is_full(self):
        is_bounded_queue = self.max_size > 0
        is_full = self.max_size == len(self.dist_list)
        return is_bounded_queue and is_full

    def _get_sorted_indexes(self):
        sorted_indexes = np.argsort(self.dist_list)
        if not self.ascending:
            sorted_indexes = sorted_indexes[::-1]
        return sorted_indexes

    def _farest_index(self):
        if len(self.dist_list) == 0:
            return 0

        sorted_indexes = self._get_sorted_indexes()
        return sorted_indexes[-1]


    def push(self, dist, data_index):
        if self.is_full():
            insertion_index = self._farest_index()
            self.dist_list[insertion_index] = dist
            self.data_index_list[insertion_index] = data_index
        else:
            self.dist_list.append(dist)
            self.data_index_list.append(data_index)

    def __iter__(self):
        sorted_indexes = np.argsort(self.dist_list)
        for index in sorted_indexes:
            yield self.data_index_list[index], self.dist_list[index]

    def to_list(self, sorted=True):
        if sorted:
            sorted_indexes = self._get_sorted_indexes()
            return [(self.data_index_list[index], self.dist_list[index]) for index in sorted_indexes]
        return list(zip(self.data_index_list, self.dist_list))
