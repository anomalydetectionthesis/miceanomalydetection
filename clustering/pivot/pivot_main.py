import os

import pandas as pd

from clustering.bow_score_model import BowScoreModel
from clustering.clustering_model import ClusteringModel
from clustering.dist_score_model import DistScoreModel
from clustering.mice_clustering_evaluation import ClusteringEvaluation
from clustering.mouse_clustering_dataset import MouseClusteringDataset
from clustering.pivot.pivot_clustering import PivotClustering
from clustering.pivot.pivot_filtering import PivotFiltering
from clustering.pivot.pivot_selection import PivotSelection
from clustering.utils.clusters_stats import ClustersStats
from dataset.prediction_split import PredictionSplit

from paths import ROOT_PATH
from stats_utils import show_df

sf = 10
pool_type = 'avg'

latent_data_path = os.path.join(ROOT_PATH, f'data/mice/frame_prediction/sf{sf}/latent_results')
pivot_data_path = os.path.join(ROOT_PATH, 'data/mice/clustering/pivot')


def _get_pivot_clustering_model(data_dict):
    pivot_file_path = os.path.join(pivot_data_path, f'sf{sf}_{pool_type}.csv')

    print("=" * 10, "Select Pivots", "=" * 10)

    if not os.path.exists(pivot_file_path):
        pivot_df = PivotSelection(n=1000, sim_th=0.98)(data_dict)
        pivot_df.to_csv(pivot_file_path, index=False)

    assert os.path.exists(pivot_file_path)

    pivot_df: pd.DataFrame = pd.read_csv(pivot_file_path)
    pivot_dict = {key: data_dict[key] for key in pivot_df['pivot_key']}
    return PivotClustering(pivot_dict)


def _get_clusters_stats(model: ClusteringModel, data_dict):
    stats_file_path = os.path.join(pivot_data_path, f'sf{sf}_{pool_type}_stats.pkl')

    print("=" * 10, "Compute Clusters Stats", "=" * 10)
    if not os.path.exists(stats_file_path):
        clusters_stats = ClustersStats.from_model(model, data_dict)
        clusters_stats.save(pivot_data_path, f'sf{sf}_{pool_type}_stats')

    assert os.path.exists(stats_file_path)
    return ClustersStats.load(stats_file_path)


if __name__ == '__main__':
    clustering_ds = MouseClusteringDataset.from_folder(latent_data_path, f'{pool_type}', video_list=PredictionSplit.full)
    print('Load clustering data dictionary')
    data_dict = clustering_ds.to_dict()

    model: PivotClustering = _get_pivot_clustering_model(data_dict)
    clusters_stats: ClustersStats = _get_clusters_stats(model, data_dict)

    filtered_pivot_df = PivotFiltering(size_th=200, classification_th=0.7, mouse_overfit_th=0.4, cage_overfit_th=0.55)(
        clusters_stats)

    show_df(filtered_pivot_df.sort_values(
        by=['pivot_class', 'pivot_pnd', 'classification_score'],
        ascending=[False, True, True]))

    print("Bow Evaluation")
    bow_pivot_dict = {key: data_dict[key] for key in filtered_pivot_df['pivot']}
    bow_model = PivotClustering(bow_pivot_dict)
    bow_score_df = BowScoreModel(bow_model, clustering_ds).get_score_df(clustering_ds)
    ClusteringEvaluation.evaluate(bow_score_df)

    print("Dist Evaluation")
    dist_pivot_df = filtered_pivot_df[filtered_pivot_df['pivot_class'] == 'abnormal']
    show_df(dist_pivot_df)
    dist_pivot_dict = {key: data_dict[key] for key in dist_pivot_df['pivot']}
    dist_model = PivotClustering(dist_pivot_dict)
    dist_score_df = DistScoreModel(dist_model, train_label=1).get_score_df(clustering_ds)

    ClusteringEvaluation.evaluate(dist_score_df)
