import numpy as np
import pandas as pd
from clustering.pivot.similarity_dictionary import SimilarityDictionary
from clustering.pivot.similarity_utils import validate_sim_fn, get_sim_fn


class PivotSelection:
    def __init__(self, n: int, sim_th=0.98, sim_fn='dot'):
        """
        :param n: The maximum number of pivots to be selected
        :param sim_th: Discard features that with similarity to the nearest pivot >= sim_th.
                       This is an optimization that reduce the number of similarity to compute at each iteration.
                       1 means no optimization.
        :param sim_fn: Determine how to compute similarity. 'dot'(Dot product) should be used with unit norm features
        """

        validate_sim_fn(sim_fn)

        self.n = n
        self.sim_th = sim_th

        self.sim_fn = get_sim_fn(sim_fn)

    def __call__(self, data_dict: dict, random_state=4859323):
        rng = np.random.RandomState(random_state)

        pivot_list = []

        print("Initialize sim dict")
        sim_dict = SimilarityDictionary(data_dict.keys())

        print("select first pivot")
        first_pivot = self._select_first_pivot(data_dict, sim_dict, rng)
        pivot_list.append(first_pivot)

        self._update_similarities(data_dict, first_pivot, sim_dict)
        possible_pivots = self._get_possible_pivot_list(sim_dict)

        while len(possible_pivots) > 0 and len(pivot_list) < self.n:
            pivot_key = rng.choice(sorted(possible_pivots))

            sim_dict.remove_latent_index(pivot_key)
            pivot_list.append(pivot_key)
            print(len(pivot_list))

            self._update_similarities(data_dict, pivot_key, sim_dict)
            possible_pivots = self._get_possible_pivot_list(sim_dict)

        pivot_list = sorted(pivot_list)
        return pivot_list

    def _select_first_pivot(self, data_dict: dict, sim_dict, rng):
        rnd_key = rng.choice(list(data_dict.keys()))
        sim_dict.remove_latent_index(rnd_key)

        best_sim, best_key = 1, None
        for data_key, sim in sim_dict:
            curr_sim = self.sim_fn(data_dict[rnd_key], data_dict[data_key])
            if curr_sim < best_sim:
                best_sim, best_key = curr_sim, data_key

        assert best_key is not None
        sim_dict.remove_latent_index(best_key)
        return best_key

    def _update_similarities(self, data_dict, pivot_key, sim_dict: SimilarityDictionary):
        for data_key, sim in sim_dict:
            new_sim = self.sim_fn(data_dict[pivot_key], data_dict[data_key])
            if new_sim > sim:
                sim_dict.change_sim(data_key, new_sim)

    def _get_possible_pivot_list(self, sim_dict):
        similarities = sim_dict.get_sorted_similarities()

        if self.sim_th is not None:
            sim_dict.filter_similarites(self.sim_th)
            similarities = sim_dict.get_sorted_similarities()

        if len(similarities) > 0:
            lower_similarity = similarities[0]
            print('lower_sim', lower_similarity)
            return sim_dict.get_latent_indexes(lower_similarity)

        return []


    @staticmethod
    def main(latent_ds, n_pivots, sim_th=0.98):
        data_dict = latent_ds.to_dict()

        pivot_selection = PivotSelection(n_pivots, sim_th=sim_th)
        pivot_key_list = pivot_selection(data_dict)
        pivot_df = pd.DataFrame({'pivot_key': pivot_key_list})
        return pivot_df
        # pivot_df.to_csv('sf5_avg.csv', index=False)
        # pivot_df = pd.read_csv('../sf5_avg.csv')


        # latent_data.show_pca(force_recompute=True)
        # latent_data.show_tsne(force_recompute=True)