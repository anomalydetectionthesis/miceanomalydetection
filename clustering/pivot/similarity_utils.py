import numpy as np


def validate_sim_fn(sim_fn):
    assert sim_fn in ['dot']


def get_sim_fn(sim_fn):
    if sim_fn == 'dot':
        return lambda ld1, ld2: np.dot(ld1, ld2)
    else:
        return lambda ld1, ld2: np.linalg.norm(np.diff(ld1, ld2), ord=2)
