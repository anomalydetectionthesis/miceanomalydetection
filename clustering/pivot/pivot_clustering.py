import os

from clustering.clustering_model import ClusteringModel
from clustering.mouse_clustering_dataset import MouseClusteringDataset
from clustering.pivot.pivot_filtering import PivotFiltering
from clustering.pivot.similarity_utils import validate_sim_fn, get_sim_fn
import numpy as np
import pandas as pd

from clustering.utils.clusters_stats import ClustersStats
from dataset.prediction_split import PredictionSplit
from paths import ROOT_PATH
from progress_bar import Progress
from stats_utils import show_df


class PivotClustering(ClusteringModel):
    def __init__(self, pivot_dict: dict, sim_fn='dot'):
        validate_sim_fn(sim_fn)
        self.pivot_dict = pivot_dict
        self.label_index_dict = {label: index for index, label in enumerate(self.pivot_dict.keys())}
        self.sim_fn = get_sim_fn(sim_fn)

    @property
    def n_clusters(self):
        return len(self.pivot_dict)

    @property
    def cluster_labels(self):
        return list(self.pivot_dict.keys())

    def predict(self, X: np.ndarray, progress=False, return_index=False, **kwargs):
        assert X.ndim in [1,2]

        if X.ndim == 1:
            X = np.array([X])
            # nearest_pivot_key, max_sim = self._select_nearest_pivot(X)
            # return self.pivot_label_dict[nearest_pivot_key], max_sim

        labels = []
        sqr_dist = []
        for vect in Progress(X, show=progress):
            nearest_pivot_key, max_sim = self._select_nearest_pivot(vect)
            if return_index:
                labels.append(self.label_index_dict[nearest_pivot_key])
            else:
                labels.append((nearest_pivot_key))
            sqr_dist.append(2*(1-max_sim))  # convert similarity to sqr_distance
        return labels, sqr_dist

    def _select_nearest_pivot(self, data: np.ndarray, random_state=4503959):
        max_sim, nearest_pivot_keys = 0, []
        for pivot_key, pivot in self.pivot_dict.items():
            curr_sim = self.sim_fn(pivot, data)
            if curr_sim > max_sim:
                max_sim = curr_sim
                nearest_pivot_keys = [pivot_key]
            elif curr_sim == max_sim:
                nearest_pivot_keys.append(pivot_key)

        assert len(nearest_pivot_keys) >= 1

        if len(nearest_pivot_keys) > 1:
            rng = np.random.RandomState(random_state)
            pivot_index = rng.randint(0, len(nearest_pivot_keys))
            nearest_pivot_key = nearest_pivot_keys[pivot_index]
        else:
            nearest_pivot_key = nearest_pivot_keys[0]

        return nearest_pivot_key, max_sim

if __name__ == '__main__':
    sf = 10
    pool_type = 'avg'

    latent_data_path = os.path.join(ROOT_PATH, f'data/mice/frame_prediction/sf{sf}/latent_results')
    pivot_data_path = os.path.join(ROOT_PATH, 'data/mice/clustering/pivot')

    clustering_ds = MouseClusteringDataset.from_folder(latent_data_path, f'{pool_type}', video_list=PredictionSplit.test)
    print('Load clustering data dictionary')
    data_dict = clustering_ds.to_dict()

    print('Load pivots')
    pivot_df: pd.DataFrame = pd.read_csv(os.path.join(pivot_data_path, f'sf{sf}_{pool_type}.csv'))
    #pivot_df: pd.DataFrame = pd.read_csv(os.path.join(pivot_data_path, f'sf{sf}_{pool_type}_filtered.csv'))

    pivot_dict = {key: data_dict[key] for key in pivot_df['pivot_key']}

    model = PivotClustering(pivot_dict)
    clusters_stats = ClustersStats.from_model(model, data_dict)
    #clusters_stats.save('/home/edo/Desktop', 'new_clusters_stats')

    #clusters_stats = ClustersStats.load('/home/edo/Desktop/new_clusters_stats')
    filtered_pivot_df = PivotFiltering(size_th=200, classification_th=0.7, mouse_overfit_th=0.4, cage_overfit_th=0.55)(clusters_stats)

    show_df(filtered_pivot_df.sort_values(
        by=['pivot_class', 'pivot_pnd', 'classification_score'],
        ascending=[False, True, True]))
