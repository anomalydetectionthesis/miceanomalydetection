import os

import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.axis import Axis

from clustering.pivot.pivot_filtering import PivotFiltering
from clustering.utils.clusters_stats import ClustersStats
from paths import ROOT_PATH


def plot_stats(pivot_key, pivot_dict):
    stats_class_list = ['normal', 'abnormal']
    stats_list = ['cage_stats', 'mice_stats', 'pnd_stats']
    stats_labels_list = ['cage', 'mice', 'pnd']


    fig, axs = plt.subplots(nrows=1, ncols=len(stats_list), figsize=(24, 8))

    # gridspec_kw = {'hspace': 3, 'wspace': 0.5}

    for stat_index in range(len(stats_list)):
        stats_dict = pivot_dict[stats_list[stat_index]]

        normal_mice_keys = set(stats_dict['normal'].keys())
        abnormal_mice_keys = set(stats_dict['abnormal'].keys())
        stats_keys = sorted(list(normal_mice_keys | abnormal_mice_keys)) # set union

        data_list = []
        for stat_class in range(len(stats_class_list)):
            class_type = stats_class_list[stat_class]
            data_list.append([])

            for stat_key in stats_keys:
                data = stats_dict[class_type].get(stat_key, 0)
                data_list[stat_class].append(data)

        index = stats_class_list
        df = pd.DataFrame(data_list, index=index, columns=[f'{stats_labels_list[stat_index]}_{key}' for key in stats_keys])

        df.plot(kind='bar',title=stats_list[stat_index], ax=axs[stat_index])

    fig.suptitle(pivot_key, fontsize=18)
    #fig.subplots_adjust(bottom=0.1, right=0.8, top=0.9, hspace=3, wspace=2)
    fig.tight_layout(rect=[0,0,0.5,0.45])

    fig.show()
    plt.close(fig)


if __name__ == '__main__':
    sf = 10
    pool_type = 'avg'

    latent_data_path = os.path.join(ROOT_PATH, f'data/mice/frame_prediction/sf{sf}/latent_results')
    pivot_data_path = os.path.join(ROOT_PATH, 'data/mice/clustering/pivot')
    stats_file_path = os.path.join(pivot_data_path, f'sf{sf}_{pool_type}_stats.pkl')
    clusters_stats = ClustersStats.load(stats_file_path)

    filtered_pivot_df = PivotFiltering(size_th=200, classification_th=0.7, mouse_overfit_th=0.4, cage_overfit_th=0.55)(clusters_stats)
    filtered_pivot_keys = filtered_pivot_df['pivot'].tolist()

    pivot_key = filtered_pivot_keys[1]
    plot_stats(pivot_key, clusters_stats.clusters[pivot_key])