import pandas as pd
import numpy as np
from sklearn.decomposition import PCA
from sklearn.manifold import TSNE


def plot_analisys_df(analisys_df: pd.DataFrame):
    import matplotlib.pyplot as plt
    import matplotlib.style as style
    style.use('ggplot')

    fig = plt.figure()
    ax = fig.add_subplot(111)

    labels = analisys_df['label'].unique()
    for label in labels:
        label_data = analisys_df[analisys_df['label'] == label]
        ax.scatter(label_data['x'].values, label_data['y'].values, s=30, label=label, alpha=0.3)

    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.grid(True)
    ax.legend()

    fig.show()

def pca_analisys(data_list: np.ndarray, class_list: np.ndarray) -> pd.DataFrame:
    pca = PCA(n_components=2)
    pca_transform = pca.fit_transform(data_list)
    pca_data = pd.DataFrame({
        'x': pca_transform[:, 0],
        'y': pca_transform[:, 1],
        'label': class_list
    })

    return pca_data


def tsne_analisys(data_list: np.ndarray, class_list: np.ndarray) -> pd.DataFrame:
    tsne = TSNE(n_components=2, verbose=1, perplexity=30, n_iter=300)
    tsne_transform = tsne.fit_transform(data_list)
    tsne_data = pd.DataFrame({
        'x': tsne_transform[:, 0],
        'y': tsne_transform[:, 1],
        'label': class_list
    })
    return tsne_data