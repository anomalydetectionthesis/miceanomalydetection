import os
import pickle
import numpy as np

from clustering.clustering_model import ClusteringModel
from clustering.pivot.sorted_queue import SortedQueue
from utils.video_name_info_extractor import video_info


class ClustersStats:
    def __init__(self, clusters_stats: dict=None):
        if clusters_stats is None:
            clusters_stats = {}
        self.clusters = clusters_stats

    def _compute_cluster_stats(self, cluster_label, neighbors_list: list, neighbors_dist: list):
        assert cluster_label not in self.clusters

        cluster_stats_dict = self._add_empty_cluster_stats_dict(cluster_label, neighbors_list, neighbors_dist)

        for neighbor_key in cluster_stats_dict['neighbors']:
            mouse_id, mouse_class, cage, geno, pnd, sex = video_info(neighbor_key).values()

            stats_update_dict = {
                'cage_stats': cage,
                'mice_stats': mouse_id,
                'pnd_stats': pnd,
                'sex_stats': sex,
                'geno_stats': geno
            }

            cluster_stats = cluster_stats_dict['cluster_stats']
            cluster_stats['total'] += 1
            cluster_stats[mouse_class] += 1

            for stats_type, stats_field in stats_update_dict.items():
                self._update_stat_field(cluster_stats_dict, stats_type, mouse_class, stats_field)

    def _update_stat_field(self, cluster_stats_dict, stats_type, mouse_class, field_name):
        stats_dict = cluster_stats_dict[stats_type][mouse_class]
        stats_dict[field_name] = stats_dict.get(field_name, 0) + 1

    def _add_empty_cluster_stats_dict(self, cluster_label, neighbors_list, neighbors_distance):
        self.clusters[cluster_label] = {
            'neighbors': neighbors_list,
            'distance': neighbors_distance,
            'cluster_stats': {
                'total': 0,
                'normal': 0,
                'abnormal': 0,
                'max_dist': -1 if len(neighbors_distance) == 0 else np.max(neighbors_distance),
                'min_dist': -1 if len(neighbors_distance) == 0 else np.min(neighbors_distance),
                'avg_dist': -1 if len(neighbors_distance) == 0 else np.average(neighbors_distance)
            },
            'cage_stats': {'normal': {}, 'abnormal': {}},
            'mice_stats': {'normal': {}, 'abnormal': {}},
            'pnd_stats': {'normal': {}, 'abnormal': {}},
            'sex_stats': {'normal': {}, 'abnormal': {}},
            'geno_stats': {'normal': {}, 'abnormal': {}},
        }

        return self.clusters[cluster_label]

    def map_stats(self, map_fn):
        stats_list = []
        for cluster_label, cluster_stats in self.clusters.items():
            stats_list.append(map_fn(cluster_label, cluster_stats))
        return stats_list

    def save(self, folder_path, file_name):
        assert os.path.exists(folder_path)
        assert file_name is not None

        with open(os.path.join(folder_path, f'{file_name}.pkl'), 'wb') as file:
            pickle.dump(self.clusters, file)

    @staticmethod
    def from_model(model: ClusteringModel, data_dict: dict) -> 'ClustersStats':
        data_keys = list(data_dict.keys())
        data_values = np.array(list(data_dict.values()))
        predicted_labels, distances = model.predict(data_values)

        assert len(predicted_labels) == len(data_values)

        # initialize pivot neighborhood data structure
        nn_dict = {label: SortedQueue() for label in model.cluster_labels}

        for index in range(len(data_dict)):
            pivot_queue: SortedQueue = nn_dict[predicted_labels[index]]
            pivot_queue.push(distances[index], data_keys[index])

        nn_dict = {key: sorted_queue.to_list() for key, sorted_queue in nn_dict.items()}

        clusters_stats = ClustersStats()
        for cluster_label, sorted_nn_list in nn_dict.items():
            neighbors_list = [key for key, dist in sorted_nn_list]
            neighbors_dist = [dist for key, dist in sorted_nn_list]
            clusters_stats._compute_cluster_stats(cluster_label, neighbors_list, neighbors_dist)

        return clusters_stats

    @staticmethod
    def load(file_path) -> 'ClustersStats':
        assert os.path.exists(file_path)
        with open(file_path, 'rb') as file:
            clusters_stats_dict: dict = pickle.load(file)
        return ClustersStats(clusters_stats_dict)

    # def get_evaluation_multi_indexes(self, pivot_key, stats_key):
    #     pivot_dict = self.clusters[pivot_key]
    #     stats_dict = pivot_dict[stats_key]
    #
    #     index_list = ['normal', 'abnormal']
    #     data_list = []
    #     s_keys = list(set(stats_dict['normal'].keys()).union(set(stats_dict['abnormal'].keys())))
    #     cols_list=[[stats_key]*len(s_keys), s_keys]
    #
    #     print(len(index_list))
    #     for row in range(len(index_list)):
    #         class_type = index_list[row]
    #         data_list.append([])
    #         for col in range(len(cols_list[1])):
    #             stat_name = cols_list[1][col]
    #             data = stats_dict[class_type].get(stat_name,0)
    #             data_list[row].append(data)
    #
    #     #index = pd.MultiIndex.from_arrays(index_list)
    #     index = index_list
    #     cols = pd.MultiIndex.from_arrays(cols_list)
    #     df = pd.DataFrame(data_list, index=index, columns=cols)
    #     df.plot(kind='bar',title=pivot_key)
    #     plt.show()
    #     print(df)
    #
    #
    # def cluster_evaluation_df(self, pivot_key):
    #     pivot_dict = self.clusters[pivot_key]
    #     pivot_id, pivot_class, pivot_cage, pivot_geno, pivot_pnd, pivot_sex = pivot_dict['info'].values()
    #
    #     # normal_pnd_dict = {('pnd',key): val for key, val in pivot_dict['pnd_stats']['normal'].items()}
    #     # abnormal_pnd_dict = {('pnd',key): val for key, val in pivot_dict['pnd_stats']['abnormal'].items()}
    #     # normal_cage_dict = {('cage', key): val for key, val in pivot_dict['cage_stats']['normal'].items()}
    #     # abnormal_cage_dict = {('cage', key): val for key, val in pivot_dict['cage_stats']['abnormal'].items()}
    #     # normal_mice_dict = {('mice', key): val for key, val in pivot_dict['mice_stats']['normal'].items()}
    #     # abnormal_mice_dict = {('mice', key): val for key, val in pivot_dict['mice_stats']['abnormal'].items()}
    #
    #     print(self.get_evaluation_multi_indexes(pivot_key, 'pnd_stats'))
    #
    #
    #     # eval_df = pd.DataFrame([
    #     #     {**normal_pnd_dict, ** normal_cage_dict, **normal_mice_dict},
    #     #     {**abnormal_pnd_dict, **abnormal_cage_dict, **abnormal_mice_dict}
    #     # ],
    #     #     index=['normal','abnormal'])
    #
    #     # eval_mi = pd.MultiIndex.from_frame(eval_df)
    #     # print(eval_mi)
    #     # return eval_df
