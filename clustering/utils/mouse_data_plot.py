import numpy as np

from clustering.utils.data_visualization import plot_analisys_df, tsne_analisys, pca_analisys
from sklearn.utils._random import sample_without_replacement


class MouseDataPlot:
    def __init__(self, latent_vecs, video_classes):
        self.latent_vecs = latent_vecs
        self.video_classes = video_classes

        # print(self.latent_vecs.shape)
        # print(self.video_class.shape)
        assert len(self.latent_vecs) == len(self.video_classes)

    def filter(self, sim_th, random_state=4859323) -> 'MouseDataPlot':
        assert sim_th > 0 and sim_th<=1

        rng = np.random.RandomState(random_state)

        keep = []
        discard = []
        to_parse = set(range(len(self.latent_vecs)))

        print(f"dataset filtering with sim_th: {sim_th}")
        while len(to_parse) > 0:
            current = rng.choice(sorted(list(to_parse)))
            to_parse.remove(current)
            keep.append(current)
            print(len(keep))
            for index in to_parse:
                sim = np.dot(self.latent_vecs[current], self.latent_vecs[index])
                assert sim.ndim == 0
                if sim.item() > sim_th:
                    discard.append(index)

            to_parse -= (set(discard))

        keep.sort()
        return MouseDataPlot(self.latent_vecs[keep], self.video_classes[keep])


    def show_pca(self, n_samples=None):
        indexes = self._get_random_indexes(n_samples)
        data_list = self.latent_vecs[indexes]
        class_list = self.video_classes[indexes]
        pca_df = pca_analisys(data_list, class_list)

        plot_analisys_df(pca_df)

    def show_tsne(self, n_samples=None):
        indexes = self._get_random_indexes(n_samples)
        data_list = self.latent_vecs[indexes]
        class_list = self.video_classes[indexes]
        tsne_df = tsne_analisys(data_list, class_list)

        plot_analisys_df(tsne_df)


    def _get_random_indexes(self, n_samples=None, random_state=4859323):
        if n_samples is None:
            n_samples = len(self.latent_vecs)

        assert n_samples <= len(self.latent_vecs)
        return sample_without_replacement(n_population=len(self.latent_vecs), n_samples=n_samples, random_state=random_state)

    def __len__(self):
        return len(self.latent_vecs)


# if __name__ == '__main__':
    # latent_data = MouseTrainDataset.from_folder('/home/edo/Documents/Tesi/MiceAnomalyDetection/data/mice/frame_prediction/sf10/latent_results', 'max')
    # latent_data.pivot(n=1000)
    #latent_data = latent_data.filter(0.94)
    #latent_data.show_pca()
    #latent_data.show_tsne()