import numpy as np
from scipy.spatial import distance

from dataset.base.dataset_model import BaseDataset
from dataset.dataset_builders.clustering_dataset import ClusteringDataset


class MouseClusteringDataset:
    def __init__(self, clustering_ds: BaseDataset, l2_norm=True):
        self.clustering_ds: BaseDataset = clustering_ds
        self.l2_norm = l2_norm
        self.videos = [vid.video_name for vid in self.clustering_ds]

    def _get_latent_vecs(self, video_name):
        video = self.clustering_ds[video_name]

        latent_vecs = video[:]
        if self.l2_norm:
            l2_norms = np.linalg.norm(latent_vecs, ord=2, axis=1)
            latent_vecs /= l2_norms[:, None]

        return latent_vecs

    def to_array(self):
        video_name = np.concatenate([[video_name]*len(self._get_latent_vecs(video_name)) for video_name in self.videos])
        video_index = np.concatenate([list(range(len(self._get_latent_vecs(video_name))))for video_name in self.videos])
        latent_vecs = np.concatenate([self._get_latent_vecs(video_name) for video_name in self.videos])


        assert len(video_name) == len(latent_vecs) and len(video_name) == len(video_index)

        return latent_vecs, video_name, video_index

    def to_dict(self, filter_fn=None):
        if filter_fn is None:
            filter_fn = lambda vid_name: True  # keep all videos

        data_dict = {}
        for video in self.videos:
            if filter_fn(video):
                latent_vecs = self._get_latent_vecs(video)
                for index in range(len(latent_vecs)):
                    data_dict[f'{video}__{index}'] = latent_vecs[index]

        return data_dict

    def __iter__(self):
        for video in self.videos:
            yield video, self._get_latent_vecs(video)

    def __len__(self):
        return len(self.videos)

    @staticmethod
    def from_folder(latent_folder, pool_type,  video_list=None) -> 'MouseClusteringDataset':
        clustering_ds = ClusteringDataset.from_hdf(latent_folder, pool=pool_type, video_list=video_list)
        return MouseClusteringDataset(clustering_ds)

if __name__ == '__main__':
    latent_data = MouseClusteringDataset.from_folder('/home/edo/Documents/Tesi/MiceAnomalyDetection/data/mice/frame_prediction/sf10/latent_results', 'avg')
    #
    # vid, latent_vecs = next(iter(latent_data))
    # v1 = latent_vecs[0]
    # v2 = latent_vecs[1]
    #
    # assert np.linalg.norm(v1) == 1
    # assert np.linalg.norm(v2) == 1
    #
    # print(1 - np.dot(v1, v2))
    # print(np.linalg.norm(v1-v2, ord=2))
    # print(distance.euclidean(v1, v2))

    # latent_data.show_pca(force_recompute=True)
    # latent_data.show_tsne(force_recompute=True)