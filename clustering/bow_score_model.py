from sklearn.linear_model import LinearRegression

from clustering.clustering_model import ClusteringModel
from clustering.mouse_clustering_dataset import MouseClusteringDataset
import numpy as np
import pandas as pd
from progress_bar import Progress


class BowScoreModel:
    def __init__(self, model: ClusteringModel, dataset: MouseClusteringDataset):
        self.model = model
        #cluster_names = [f'c_{cluster_id}' for cluster_id in range(model.n_clusters)]
        self.reg_model = self._fit_reg_model(dataset)

    def _fit_reg_model(self, dataset):
        labels = []
        bows = []
        for video_name, latent_data in Progress(dataset):
            bow = self.compute_bow(latent_data)

            labels.append(self._get_label(video_name))
            bows.append(bow)

        reg_model = LinearRegression()
        reg_model.fit(bows, labels)
        return reg_model
        # w = reg.coef_
        # y = reg.predict(bows)
        # w_norm = np.linalg.norm(w, ord=2)
        # dist = y / w_norm # compute distance from the input bow and the regression plan

    def compute_bow(self, latent_data: np.ndarray):
        n_classes = self.model.n_clusters
        labels, _ = self.model.predict(latent_data, progress=False, return_index=True)

        classes, counts = np.unique(labels, return_counts=True)
        assert max(classes) < n_classes

        bow = np.zeros(n_classes)  # [0] * n_classes
        bow[classes] = counts
        return bow

    def compute_score(self, latent_data):
        bow = self.compute_bow(latent_data)
        if bow.ndim == 1:
            bow = np.expand_dims(bow, axis=0)
        score = self.reg_model.predict(bow)
        return score

    def get_score_df(self, dataset) -> pd.DataFrame:
        df_data = []
        for video_name, latent_data in dataset:
            df_data.append({
                'video': video_name,
                'label': self._get_label(video_name),
                'score': self.compute_score(latent_data)
            })

        return pd.DataFrame(df_data)


    def _get_label(self,video_name):
        return 0 if 'wt' in video_name else 1