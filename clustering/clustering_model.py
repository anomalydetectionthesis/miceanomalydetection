from abc import ABC, abstractmethod
import numpy as np

class ClusteringModel(ABC):

    @property
    @abstractmethod
    def n_clusters(self):
        pass

    @property
    @abstractmethod
    def cluster_labels(self):
        pass

    @abstractmethod
    def predict(self, X: np.ndarray, progress=False, **kwargs):
        '''
        Args:
            -X: single value or list of values to predict
        Return:
            -label: the nearest centroid for each input data,
            -sqr_dist: (euclidian distance) ^ 2
        '''
        pass